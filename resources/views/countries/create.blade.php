@extends('backoffice.master')
@section('content')
    <!--== breadcrumbs ==-->
    <div class="sb2-2-2">
        <ul>
            <li><a href="{{route('index')}}"><i class="fa fa-home" aria-hidden="true"></i> Home</a> </li>
            <li class="active-bre"><a href="#"> Add Listing Category</a> </li>
            <li class="page-back"><a href="#"><i class="fa fa-backward" aria-hidden="true"></i> Back</a> </li>
        </ul>
    </div>
    <div class="tz-2 tz-2-admin">
        <div class="tz-2-com tz-2-main">
            <h4>Add New Country</h4> <a class="dropdown-button drop-down-meta drop-down-meta-inn" href="#" data-activates="dr-list"><i class="material-icons">more_vert</i></a>
{{--            <ul id="dr-list" class="dropdown-content">--}}
{{--                <li><a href="#!">Add New</a> </li>--}}
{{--                <li><a href="#!">Edit</a> </li>--}}
{{--                <li><a href="#!">Update</a> </li>--}}
{{--                <li class="divider"></li>--}}
{{--                <li><a href="#!"><i class="material-icons">delete</i>Delete</a> </li>--}}
{{--                <li><a href="#!"><i class="material-icons">subject</i>View All</a> </li>--}}
{{--                <li><a href="#!"><i class="material-icons">play_for_work</i>Download</a> </li>--}}
{{--            </ul>--}}
            <!-- Dropdown Structure -->
            <div class="split-row">
                <div class="col-md-12">
                    <div class="box-inn-sp ad-mar-to-min">
                        <div class="tab-inn ad-tab-inn">
                            <div class="tz2-form-pay tz2-form-com">
                                <form action="{{route('countries.store')}}" method="post" enctype="multipart/form-data" >
                                    @csrf
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <input type="text" name="name" class="validate"/>
                                            <label>Country Name</label>
                                        </div>
                                    </div>
{{--                                    <div class="row">--}}
{{--                                        <div class="input-field col s12">--}}
{{--                                            <select name="locations[]" multiple>--}}
{{--                                                <option value="" readonly="">Select Location</option>--}}
{{--                                                @foreach($locations as $location)--}}
{{--                                                    <option value="{{$location->id}}">{{$location->name}}</option>--}}
{{--                                                @endforeach--}}
{{--                                            </select>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <button type="submit" class="waves-effect waves-light full-btn">Submit</button></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page_level_scripts')
    <script type="text/javascript">
        $.notify("{{Session::get('message')}}", "{{Session::get('icon')}}");
    </script>

@endsection