@extends('backoffice.master')
@section('content')
<section>
    <style>
        .tz-l {
            float: left;
            width: 25%!important;
        }
        .tz-2 {
            float: left;
            width: 70%!important;
        }
    </style>
    <div class="tz">
        <!--LEFT SECTION-->
        <div class="tz-l">
            <div class="tz-l-1">
                <ul>
                    <li><img src="{{asset('users/'.$user->profile_photo_path)}}" alt="" /> </li>
                    <li><span>80%</span> profile compl</li>
                    <li><span>18</span> Notifications</li>
                </ul>
            </div>
            <!--				<div class="tz-l-2">-->
            <!--					<ul>-->
            <!--						<li>-->
            <!--							<a href="dashboard.html"><img src="images/icon/dbl1.png" alt="" /> My Dashboard</a>-->
            <!--						</li>-->
            <!--						<li>-->
            <!--							<a href="db-all-listing.html"><img src="images/icon/dbl2.png" alt="" /> All Listing</a>-->
            <!--						</li>-->
            <!--						<li>-->
            <!--							<a href="db-listing-add.html"><img src="images/icon/dbl3.png" alt="" /> Add New Listing</a>-->
            <!--						</li>-->
            <!--						<li>-->
            <!--							<a href="db-message.html"><img src="images/icon/dbl14.png" alt="" /> Messages(12)</a>-->
            <!--						</li>-->
            <!--						<li>-->
            <!--							<a href="db-review.html"><img src="images/icon/dbl13.png" alt="" /> Reviews(05)</a>-->
            <!--						</li>-->
            <!--						<li>-->
            <!--							<a href="db-my-profile.html" class="tz-lma"><img src="images/icon/dbl6.png" alt="" /> My Profile</a>-->
            <!--						</li>-->
            <!--						<li>-->
            <!--							<a href="db-post-ads.html"><img src="images/icon/dbl11.png" alt="" /> Ad Summary</a>-->
            <!--						</li>-->
            <!--						<li>-->
            <!--							<a href="db-payment.html"><img src="images/icon/dbl9.png" alt=""> Check Out</a>-->
            <!--						</li>-->
            <!--						<li>-->
            <!--							<a href="db-invoice-all.html"><img src="images/icon/db21.png" alt="" /> Invoice</a>-->
            <!--						</li>						-->
            <!--						<li>-->
            <!--							<a href="db-claim.html"><img src="images/icon/dbl7.png" alt="" /> Claim & Refund</a>-->
            <!--						</li>-->
            <!--						<li>-->
            <!--							<a href="db-setting.html"><img src="images/icon/dbl210.png" alt="" /> Setting</a>-->
            <!--						</li>-->
            <!--						<li>-->
            <!--							<a href="#!"><img src="images/icon/dbl12.png" alt="" /> Log Out</a>-->
            <!--						</li>-->
            <!--					</ul>-->
            <!--				</div>-->
        </div>
        <!--CENTER SECTION-->
        <div class="tz-2">
            <div class="tz-2-com tz-2-main">
                <h4>Profile</h4>
                <div class="db-list-com tz-db-table">
                    <div class="ds-boar-title">
                        <h2>Edit Profile</h2>
                        <p>All the Lorem Ipsum generators on the All the Lorem Ipsum generators on the</p>
                    </div>
                    <div class="tz2-form-pay tz2-form-com">
                        <form class="col s12" action="{{route('profileUpdate')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="input-field col s12">
                                    <input type="text" name="username" value="{{$user->name}}" class="validate">
                                    <label>User Name</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12 m6">
                                    <input type="password" name="password" class="validate">
                                    <label>Enter Password</label>
                                </div>
                                <div class="input-field col s12 m6">
                                    <input type="password" name="password_confirmation" class="validate">
                                    <label>Confirm password</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12 m6">
                                    <input type="email" name="email" value="{{$user->email}}" class="validate">
                                    <label>Email id</label>
                                </div>
                                <div class="input-field col s12 m6">
                                    <input type="tel" name="phone" value="{{$user->phone}}" class="validate">
                                    <label>Phone</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <select name="status">
                                        <option value="" disabled selected>Select Status</option>
                                        <option value="1" {{$user->status==1 ? 'selected':''}}>Active</option>
                                        <option value="2" {{$user->status==2 ? 'selected':''}}>Non-Active</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input type="date" name="dob" value="{{\Carbon\Carbon::parse($user->dob)->toDateString()}}" class="validate">
{{--                                    <label>Date Of Birth</label>--}}
                                </div>
                            </div>
                            <div class="row tz-file-upload">
                                <div class="file-field input-field">
                                    <div class="tz-up-btn"> <span>File</span>
                                        <input type="file" name="profile_picture"> </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text"> </div>
                                </div>
                                <h3>Old Picture</h3>
                                <img src="{{asset('users/'.$user->profile_photo_path)}}" class="col-md-8">
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <button type="submit" class="waves-effect waves-light full-btn">SUBMIT</button></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
