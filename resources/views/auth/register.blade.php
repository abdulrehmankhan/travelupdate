@extends('frontend.master')
@section('content')

        <!-- START Register Modal -->

{{--            <div class="modal-dialog modal-dialog-centered" role="document">--}}
{{--                <div class="modal-content ">--}}
{{--                    <div class="modal-body">--}}

{{--                        <div class="register-form">--}}
{{--                            <x-jet-validation-errors class="mb-4" />--}}
{{--                            <form method="POST" action="{{ route('register') }}">--}}
{{--                                @csrf--}}
{{--                            <div class="form-group">--}}
{{--                                <input type="text" name="name" class="form-control form-control-lg" placeholder="Full name"/>--}}
{{--                                <!-- /.form-control -->--}}
{{--                            </div>--}}
{{--                            <!-- /.form-group -->--}}
{{--                            <div class="form-group">--}}
{{--                                <input type="email" name="email" class="form-control form-control-lg" placeholder="Email"/>--}}
{{--                                <!-- /.form-control -->--}}
{{--                            </div>--}}
{{--                            <!-- /.form-group -->--}}

{{--                            <div class="form-group">--}}
{{--                                <input type="password" name="password" class="form-control form-control-lg" placeholder="Enter password"/>--}}
{{--                                <!-- /.form-control -->--}}
{{--                            </div>--}}
{{--                            <!-- /.form-group -->--}}
{{--                            <div class="form-group">--}}
{{--                                <input type="password" name="password_confirmation" class="form-control form-control-lg" placeholder="Enter password"/>--}}
{{--                                <!-- /.form-control -->--}}
{{--                            </div>--}}
{{--                            <!-- /.form-group -->--}}
{{--                                <div class="form-group">--}}
{{--                                    <label>Register As A</label>--}}
{{--                                    <select name="is_admin" class="form-control form-control-lg">--}}
{{--                                    <option value="" readonly="readonly">Select your role</option>--}}
{{--                                    <option value="1">Hotel Owner</option>--}}
{{--                                    <option value="2">Property Owner</option>--}}
{{--                                    <option value="3">User</option>--}}
{{--                                    </select>--}}
{{--                                    <!-- /.form-control -->--}}
{{--                                </div>--}}
{{--                                <!-- /.form-group -->--}}

{{--                            <div class="mt-3">--}}
{{--                                By clicking Join now, you agree to the J Travels <a href="terms.html">Terms of Service</a>, <a href="privacy.html">Privacy Policy</a>.--}}

{{--                            </div>--}}
{{--                            <!-- /.mt-3 -->--}}


{{--                                <button type="submit" class="mt-4 btn btn-secondary btn-block btn-lg">Register</button>--}}

{{--                            <!-- /.btn btn-primary -->--}}
{{--                            </form>--}}
{{--                            <br>--}}
{{--                            <hr data-title="or continue with" class="mt-4 mb-4"/>--}}
{{--                            <br>--}}
{{--                            <div class="mt-3">--}}
{{--                                <div class="row">--}}
{{--                                    <div class="col-md-6 col-lg-6 col-sm-12">--}}
{{--                                        <div class=" btn btn-outline-secondary no-round btn-block btn-lg">--}}
{{--                                            <img src="assets/icons/facebook-icon.svg" alt="google icon" class="icon mr-2"/> Facebook--}}
{{--                                        </div>--}}
{{--                                        <!-- /.btn btn-primary -->--}}
{{--                                    </div>--}}
{{--                                    <!-- /.col-md-6 col-lg-6 col-sm-12 -->--}}
{{--                                    <div class="col-md-6 col-lg-6 col-sm-12">--}}
{{--                                        <div class=" btn btn-outline-secondary no-round btn-block btn-lg">--}}
{{--                                            <img src="assets/icons/google-pluse-icon.svg" alt="google icon" class="icon mr-2"/> Google--}}
{{--                                        </div>--}}
{{--                                        <!-- /.btn btn-primary -->--}}
{{--                                    </div>--}}
{{--                                    <!-- /.col-md-6 col-lg-6 col-sm-12 -->--}}
{{--                                </div>--}}
{{--                                <!-- /.row -->--}}

{{--                            </div>--}}

{{--                            <!-- /.mt-2 -->--}}
{{--                            <div class="mt-3">--}}
{{--                                Already have an J Travels account? <a href="{{route('login')}}">Log in</a>--}}
{{--                            </div>--}}
{{--                            <!-- /.mt-2 -->--}}
{{--                        </div>--}}
{{--                        <!-- /.login-form -->--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}



        <section class="tz-register">
            <div class="log-in-pop">
                <div class="log-in-pop-left">
                    <h1>Hello... <span></span></h1>
                    <p>Don't have an account? Create your account. It's take less then a minutes</p>
                    <h4>Login with social media</h4>
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook"></i> Facebook</a>
                        </li>
                        <li><a href="#"><i class="fa fa-google"></i> Google+</a>
                        </li>
                        <li><a href="#"><i class="fa fa-twitter"></i> Twitter</a>
                        </li>
                    </ul>
                </div>
                <div class="log-in-pop-right">
                    <a href="#" class="pop-close" data-dismiss="modal"><img src="images/cancel.png" alt="" />
                    </a>
                    <h4>Create an Account</h4>
                    <p>Don't have an account? Create your account. It's take less then a minutes</p>
                    <form method="POST" action="{{ route('register') }}" class="s12">
                        @csrf
                        <div>
                            <div class="input-field s12">
                                <input type="text" data-ng-model="name1" name="name" class="validate">
                                <label>User name</label>
                            </div>
                        </div>
                        <div>
                            <div class="input-field s12">
                                <input type="email"  name="email" class="validate">
                                <label>Email id</label>
                            </div>
                        </div>
                        <div>
                            <div class="input-field s12">
                                <input type="password" name="password" class="validate">
                                <label>Password</label>
                            </div>
                        </div>
                        <div>
                            <div class="input-field s12">
                                <input type="password" name="password_confirmation" class="validate">
                                <label>Confirm password</label>
                            </div>
                        </div>
                        <div>
                            <div class="input-field s12">



                                <select name="is_admin">
                                    <option value="" disabled selected>Register Role As</option>
                                    <option value="1">Hotel Owner</option>
                                    <option value="2">Property Owner</option>
                                    <option value="3">User</option>
                                </select>
                            </div>
                        </div>
                        <div>
                            <div class="input-field s4">
                                <input type="submit" value="Register" class="waves-effect waves-light log-in-btn"> </div>
                        </div>
                        <div>
                            <div class="input-field s12"> <a href="{{route('login')}}">Are you a already member ? Login</a> </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!--MOBILE APP-->
        <section class="web-app com-padd">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 web-app-img"> <img src="images/mobile.png" alt="" /> </div>
                    <div class="col-md-6 web-app-con">
                        <h2>Looking for the Best Service Provider? <span>Get the App!</span></h2>
                        <ul>
                            <li><i class="fa fa-check" aria-hidden="true"></i> Find nearby listings</li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> Easy service enquiry</li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> Listing reviews and ratings</li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> Manage your listing, enquiry and reviews</li>
                        </ul> <span>We'll send you a link, open it on your phone to download the app</span>
                        <form>
                            <ul>
                                <li>
                                    <input type="text" placeholder="+01" /> </li>
                                <li>
                                    <input type="number" placeholder="Enter mobile number" /> </li>
                                <li>
                                    <input type="submit" value="Get App Link" /> </li>
                            </ul>
                        </form>
                        <a href="#"><img src="images/android.png" alt="" /> </a>
                        <a href="#"><img src="images/apple.png" alt="" /> </a>
                    </div>
                </div>
            </div>
        </section>
        <!-- #register-modal .modal -->
@endsection
<script>
    import Button from "../../../vendor/laravel/jetstream/stubs/inertia/resources/js/Jetstream/Button";
    import Label from "../../../vendor/laravel/jetstream/stubs/inertia/resources/js/Jetstream/Label";
    export default {
        components: {Label, Button}
    }
</script>