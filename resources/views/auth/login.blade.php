@extends('frontend.master')

@section('content')
<div class="row">

{{--    <div class="modal-dialog modal-dialog-centered" role="document">--}}
{{--        <div class="modal-content ">--}}
{{--            <div class="modal-body">--}}

{{--                <div class="login-form">--}}
{{--                    <h3 class="title">--}}
{{--                        Log in to continue--}}
{{--                        @if (session('status'))--}}
{{--                            <div class="mb-4 font-medium text-sm text-green-600">--}}
{{--                                {{ session('status') }}--}}
{{--                            </div>--}}
{{--                        @endif--}}
{{--                    </h3>--}}
{{--                    <!-- /.title -->--}}
{{--                    <form method="POST" action="{{ route('login') }}">--}}
{{--                        @csrf--}}
{{--                    <div class="form-group">--}}
{{--                        <input type="email" name="email" class="form-control form-control-lg" placeholder="Email Address"/>--}}
{{--                        <!-- /.form-control -->--}}
{{--                    </div>--}}
{{--                    <!-- /.form-group -->--}}

{{--                    <div class="form-group">--}}
{{--                        <input type="password" name="password" class="form-control form-control-lg" placeholder="Password"/>--}}
{{--                        <!-- /.form-control -->--}}
{{--                    </div>--}}
{{--                    <!-- /.form-group -->--}}

{{--                    <div class="mt-4">--}}
{{--                        <button type="submit" class="mt-4 btn btn-primary btn-block btn-lg">{{ __('Login') }}</button>--}}
{{--                    </div>--}}
{{--                    </form>--}}
{{--                    <!-- /.btn btn-primary -->--}}

{{--                    <div class="mt-3 text-center text-capitalize">--}}
{{--                        @if (Route::has('password.request'))--}}
{{--                            <a href="{{ route('password.request') }}">--}}
{{--                                {{ __('Forgot your password?') }}--}}
{{--                            </a>--}}
{{--                        @endif--}}
{{--                    </div>--}}
{{--                    <!-- /.mt-3 text-center text-capitalize -->--}}


{{--                    <hr data-title="or continue with" class="mt-4 mb-4"/>--}}
{{--                    <div class="mt-3">--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-md-6 col-lg-6 col-sm-12">--}}
{{--                                <div class=" btn btn-outline-secondary no-round btn-block btn-lg">--}}
{{--                                    <img src="assets/icons/facebook-icon.svg" alt="google icon" class="icon mr-2"/> Facebook--}}
{{--                                </div>--}}
{{--                                <!-- /.btn btn-primary -->--}}
{{--                            </div>--}}
{{--                            <!-- /.col-md-6 col-lg-6 col-sm-12 -->--}}
{{--                            <div class="col-md-6 col-lg-6 col-sm-12 mt-2 mt-lg-0 mt-sm-0">--}}
{{--                                <div class=" btn btn-outline-secondary no-round btn-block btn-lg d-flex align-items-center justify-content-center">--}}
{{--                                    <img src="assets/icons/google-pluse-icon.svg" alt="google icon" class="icon mr-2"/> Google--}}
{{--                                </div>--}}
{{--                                <!-- /.btn btn-primary -->--}}
{{--                            </div>--}}
{{--                            <!-- /.col-md-6 col-lg-6 col-sm-12 -->--}}
{{--                        </div>--}}
{{--                        <!-- /.row -->--}}

{{--                    </div>--}}

{{--                    <!-- /.mt-2 -->--}}
{{--                    <div class="mt-3">--}}
{{--                        Don’t have an account? <a href="{{route('register')}}"> Sign up</a>--}}
{{--                    </div>--}}
{{--                    <!-- /.mt-2 -->--}}
{{--                </div>--}}
{{--                <!-- /.login-form -->--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}


    <section class="tz-register">
        <div class="log-in-pop">
            <div class="log-in-pop-left">
                <h1>Hello... <span></span></h1>
                <p>Don't have an account? Create your account. It's take less then a minutes</p>
                <h4>Login with social media</h4>
                <ul>
                    <li><a href="#"><i class="fa fa-facebook"></i> Facebook</a>
                    </li>
                    <li><a href="#"><i class="fa fa-google"></i> Google+</a>
                    </li>
                    <li><a href="#"><i class="fa fa-twitter"></i> Twitter</a>
                    </li>
                </ul>
            </div>
            <div class="log-in-pop-right">
                <a href="#" class="pop-close" data-dismiss="modal"><img src="images/cancel.png" alt="" />
                </a>
                <h4>Login</h4>
                <p>Don't have an account? Create your account. It's take less then a minutes</p>
                <form method="POST" action="{{ route('login') }}" class="s12">
                    @csrf
                    <div>
                        <div class="input-field s12">
                            <input type="text" data-ng-model="email" name="email" class="validate">
                            <label>User name</label>
                        </div>
                    </div>
                    <div>
                        <div class="input-field s12">
                            <input type="password" class="validate" name="password">
                            <label>Password</label>
                        </div>
                    </div>
                    <div>
                        <div class="input-field s4">
                            <input type="submit" value="Login" class="waves-effect waves-light log-in-btn"> </div>
                    </div>
                    <div>
                        <div class="input-field s12"> <a href="{{ route('password.request') }}">Forgot password</a> | <a href="{{route('register')}}">Create a new account</a> </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!--MOBILE APP-->
    <section class="web-app com-padd">
        <div class="container">
            <div class="row">
                <div class="col-md-6 web-app-img"> <img src="images/mobile.png" alt="" /> </div>
                <div class="col-md-6 web-app-con">
                    <h2>Looking for the Best Service Provider? <span>Get the App!</span></h2>
                    <ul>
                        <li><i class="fa fa-check" aria-hidden="true"></i> Find nearby listings</li>
                        <li><i class="fa fa-check" aria-hidden="true"></i> Easy service enquiry</li>
                        <li><i class="fa fa-check" aria-hidden="true"></i> Listing reviews and ratings</li>
                        <li><i class="fa fa-check" aria-hidden="true"></i> Manage your listing, enquiry and reviews</li>
                    </ul> <span>We'll send you a link, open it on your phone to download the app</span>
                    <form>
                        <ul>
                            <li>
                                <input type="text" placeholder="+01" /> </li>
                            <li>
                                <input type="number" placeholder="Enter mobile number" /> </li>
                            <li>
                                <input type="submit" value="Get App Link" /> </li>
                        </ul>
                    </form>
                    <a href="#"><img src="images/android.png" alt="" /> </a>
                    <a href="#"><img src="images/apple.png" alt="" /> </a>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
