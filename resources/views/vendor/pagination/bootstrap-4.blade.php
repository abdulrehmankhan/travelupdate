@if ($paginator->hasPages())

        <ul class="pagination list-pagenat">
            @if ($paginator->onFirstPage())
                <li class="waves-effect disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                    <span class="waves-effect" aria-hidden="true">&lsaquo;</span>
                </li>
            @else
                <li class="waves-effect">
                    <a href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">&lsaquo;</a>
                </li>
            @endif
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="waves-effect disabled" aria-disabled="true">{{ $element }}</li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="active" aria-current="page">{{ $page }}</li>
                        @else
                            <li class="waves-effect"><a href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li class="waves-effect">
                    <a href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">&rsaquo;</a>
                </li>
            @else
                <li class="waves-effect" aria-disabled="true" aria-label="@lang('pagination.next')">
                    <span class="waves-effect" aria-hidden="true">&rsaquo;</span>
                </li>
            @endif
        </ul>
@endif
