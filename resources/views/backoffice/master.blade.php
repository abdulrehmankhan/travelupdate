<!DOCTYPE html>
<html lang="en">

<head>
    <title>World Best Local Directory Website template</title>
    <!-- META TAGS -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- FAV ICON(BROWSER TAB ICON) -->
    <link rel="shortcut icon" href="{{asset('frontend/images/fav.ico')}}" type="image/x-icon">
    <!-- GOOGLE FONT -->
    <link href="https://fonts.googleapis.com/css?family=Poppins%7CQuicksand:500,700" rel="stylesheet">
    <!-- FONTAWESOME ICONS -->
    <link rel="stylesheet" href="{{asset('frontend/css/font-awesome.min.css')}}">
    <!-- ALL CSS FILES -->
    <link href="{{asset('frontend/css/materialize.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/bootstrap.css')}}" rel="stylesheet" type="text/css" />
    <!-- RESPONSIVE.CSS ONLY FOR MOBILE AND TABLET VIEWS -->
    <link href="{{asset('frontend/css/responsive.css')}}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="{{asset('frontend/js/html5shiv.js')}}"></script>
    <script src="{{asset('frontend/js/respond.min.js')}}"></script>
    <![endif]-->
</head>

<body>
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>
<!--== MAIN CONTRAINER ==-->
<div class="container-fluid sb1">
    <div class="row">
        <!--== LOGO ==-->
        <div class="col-md-2 col-sm-3 col-xs-6 sb1-1"> <a href="#" class="btn-close-menu"><i class="fa fa-times" aria-hidden="true"></i></a> <a href="#" class="atab-menu"><i class="fa fa-bars tab-menu" aria-hidden="true"></i></a>
            <a href="{{route('index')}}" class="logo"><img src="{{asset('frontend/images/logo1.png')}}" alt="" /> </a>
        </div>
        <!--== SEARCH ==-->
        <div class="col-md-6 col-sm-6 mob-hide">
{{--            <form class="app-search">--}}
{{--                <input type="text" placeholder="Search..." class="form-control"> <a href=""><i class="fa fa-search"></i></a> </form>--}}
        </div>
        <!--== NOTIFICATION ==-->
        <div class="col-md-2 tab-hide">
{{--            <div class="top-not-cen"> <a class='waves-effect btn-noti' href='#'><i class="fa fa-commenting-o" aria-hidden="true"></i><span>5</span></a> <a class='waves-effect btn-noti' href='#'><i class="fa fa-envelope-o" aria-hidden="true"></i><span>5</span></a> <a class='waves-effect btn-noti' href='#'><i class="fa fa-tag" aria-hidden="true"></i><span>5</span></a> </div>--}}
        </div>
        <!--== MY ACCCOUNT ==-->
{{--        <div class="col-md-2 col-sm-3 col-xs-6">--}}
{{--            <!-- Dropdown Trigger -->--}}
{{--            <a class='waves-effect dropdown-button top-user-pro' href='#' data-activates='top-menu'><img src="{{asset('frontend/images/users/6.png')}}" alt="" />My Account <i class="fa fa-angle-down" aria-hidden="true"></i> </a>--}}
{{--            <!-- Dropdown Structure -->--}}
{{--            <ul id='top-menu' class='dropdown-content top-menu-sty'>--}}
{{--                <li><a href="admin-setting.html" class="waves-effect"><i class="fa fa-cogs"></i>Admin Setting</a> </li>--}}
{{--                <li><a href="admin-analytics.html"><i class="fa fa-bar-chart"></i> Analytics</a> </li>--}}
{{--                <li><a href="admin-ads.html"><i class="fa fa-buysellads" aria-hidden="true"></i>Ads</a> </li>--}}
{{--                <li><a href="admin-payment.html"><i class="fa fa-usd" aria-hidden="true"></i> Payments</a> </li>--}}
{{--                <li><a href="admin-notifications.html"><i class="fa fa-bell-o"></i>Notifications</a> </li>--}}
{{--                <li><a href="#" class="waves-effect"><i class="fa fa-undo" aria-hidden="true"></i> Backup Data</a> </li>--}}
{{--                <li class="divider"></li>--}}
{{--                <li><a href="{{ route('logout') }}"--}}
{{--                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="ho-dr-con-last waves-effect">--}}
{{--                        <i class="fa fa-sign-in" aria-hidden="true"></i> Logout--}}
{{--                    <form id="logout-form" method="POST" action="{{ route('logout') }}">--}}
{{--                        @csrf--}}
{{--                    </form> </a></li>--}}
{{--            </ul>--}}
{{--        </div>--}}
    </div>
</div>
<!--== BODY CONTNAINER ==-->
<div class="container-fluid sb2">
    <div class="row">
        @include('backoffice.includes.sidebar')
        <!--== BODY INNER CONTAINER ==-->
        <div class="sb2-2">
            @yield('content')
        </div>
    </div>
</div>

            {{--<!--SCRIPT FILES-->--}}
            <script src="{{asset('frontend/js/jquery.min.js')}}"></script>
            <script src="{{asset('frontend/js/bootstrap.js')}}" type="text/javascript"></script>
            <script src="{{asset('frontend/js/materialize.min.js')}}" type="text/javascript"></script>
            <script src="{{asset('frontend/js/custom.js')}}"></script>
            <script src="{{asset('js/notify.js')}}"></script>

@yield('page_level_scripts')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.23/b-1.6.5/b-colvis-1.6.5/b-flash-1.6.5/b-html5-1.6.5/b-print-1.6.5/datatables.min.css"/>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.23/b-1.6.5/b-colvis-1.6.5/b-flash-1.6.5/b-html5-1.6.5/b-print-1.6.5/datatables.min.js"></script>
</body>

</html>