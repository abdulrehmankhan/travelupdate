{{--<aside class="main-sidebar sidebar-dark-primary elevation-4">--}}
{{--    <!-- Brand Logo -->--}}
{{--    <a href="index3.html" class="brand-link">--}}
{{--        <img src="{{asset('backend/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"--}}
{{--             style="opacity: .8">--}}
{{--        <span class="brand-text font-weight-light">AdminLTE 3</span>--}}
{{--    </a>--}}

{{--    <!-- Sidebar -->--}}
{{--    <div class="sidebar">--}}
{{--        <!-- Sidebar user panel (optional) -->--}}
{{--        <div class="user-panel mt-3 pb-3 mb-3 d-flex">--}}
{{--            <div class="image">--}}
{{--                <img src="{{asset('backend/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">--}}
{{--            </div>--}}
{{--            <div class="info">--}}
{{--                <a href="#" class="d-block">{{\Illuminate\Support\Facades\Auth::user()->name}}</a>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <!-- Sidebar Menu -->--}}
{{--        <nav class="mt-2">--}}
{{--            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">--}}
{{--                <!-- Add icons to the links using the .nav-icon class--}}
{{--                     with font-awesome or any other icon font library -->--}}
{{--                <li class="nav-item has-treeview menu-open">--}}
{{--                    <a href="#" class="nav-link active">--}}
{{--                        <i class="nav-icon fas fa-tachometer-alt"></i>--}}
{{--                        <p>--}}
{{--                            Dashboard--}}
{{--                            <i class="right fas fa-angle-left"></i>--}}
{{--                        </p>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                @if(\Illuminate\Support\Facades\Auth::user()->is_admin==1)--}}
{{--                <li class="nav-item has-treeview">--}}
{{--                    <a href="#" class="nav-link">--}}
{{--                        <i class="nav-icon fas fa-copy"></i>--}}
{{--                        <p>--}}
{{--                            Hotels--}}
{{--                            <i class="fas fa-angle-left right"></i>--}}
{{--                            <span class="badge badge-info right">6</span>--}}
{{--                        </p>--}}
{{--                    </a>--}}
{{--                    <ul class="nav nav-treeview">--}}
{{--                        <li class="nav-item">--}}
{{--                            <a href="{{route('listings.index')}}" class="nav-link">--}}
{{--                                <i class="far fa-circle nav-icon"></i>--}}
{{--                                <p>View All</p>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item">--}}
{{--                            <a href="{{route('listings.create')}}" class="nav-link">--}}
{{--                                <i class="far fa-circle nav-icon"></i>--}}
{{--                                <p>Add New Hotel</p>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item">--}}
{{--                            <a href="{{route('listings.makeAvailability')}}" class="nav-link">--}}
{{--                                <i class="far fa-circle nav-icon"></i>--}}
{{--                                <p>Add Hotel Availability--}}
{{--                                </p>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                @elseif(\Illuminate\Support\Facades\Auth::user()->is_admin==2)--}}
{{--                <li class="nav-item has-treeview">--}}
{{--                    <a href="#" class="nav-link">--}}
{{--                        <i class="nav-icon fas fa-copy"></i>--}}
{{--                        <p>--}}
{{--                            Property--}}
{{--                            <i class="fas fa-angle-left right"></i>--}}
{{--                            <span class="badge badge-info right">6</span>--}}
{{--                        </p>--}}
{{--                    </a>--}}
{{--                    <ul class="nav nav-treeview">--}}
{{--                        <li class="nav-item">--}}
{{--                            <a href="{{route('listings.index')}}" class="nav-link">--}}
{{--                                <i class="far fa-circle nav-icon"></i>--}}
{{--                                <p>View All</p>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item">--}}
{{--                            <a href="{{route('listings.create')}}" class="nav-link">--}}
{{--                                <i class="far fa-circle nav-icon"></i>--}}
{{--                                <p>Add New Property</p>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item">--}}
{{--                            <a href="{{route('listings.makeAvailability')}}" class="nav-link">--}}
{{--                                <i class="far fa-circle nav-icon"></i>--}}
{{--                                <p>Add Property Availability--}}
{{--                                </p>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                @endif--}}
{{--                <li class="nav-item has-treeview">--}}
{{--                    <a href="#" class="nav-link">--}}
{{--                        <i class="nav-icon fas fa-copy"></i>--}}
{{--                        <p>--}}
{{--                            Categories--}}
{{--                            <i class="fas fa-angle-left right"></i>--}}
{{--                            <span class="badge badge-info right">6</span>--}}
{{--                        </p>--}}
{{--                    </a>--}}
{{--                    <ul class="nav nav-treeview">--}}
{{--                        <li class="nav-item">--}}
{{--                            <a href="{{route('categories.index')}}" class="nav-link">--}}
{{--                                <i class="far fa-circle nav-icon"></i>--}}
{{--                                <p>View All</p>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item">--}}
{{--                            <a href="{{route('categories.create')}}" class="nav-link">--}}
{{--                                <i class="far fa-circle nav-icon"></i>--}}
{{--                                <p>Add New Category</p>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--            </ul>--}}
{{--        </nav>--}}
{{--        <!-- /.sidebar-menu -->--}}
{{--    </div>--}}
{{--    <!-- /.sidebar -->--}}
{{--</aside>--}}
<div class="sb2-1">
    <!--== USER INFO ==-->
    <div class="sb2-12">
        <ul>
            @if(\Illuminate\Support\Facades\Auth::user()->profile_photo_path)
            <li><img src="{{asset('users/'.$user->profile_photo_path)}}" alt=""></li>
            @else
            <li><img src="{{asset('frontend/images/users/2.png')}}" alt=""></li>
            @endif
            <li>
                <h5>{{\Illuminate\Support\Facades\Auth::user()->name}}<span> Santa Ana, CA</span></h5></li>
            <li></li>
        </ul>
    </div>
    <!--== LEFT MENU ==-->
    <div class="sb2-13">
        <ul class="collapsible" data-collapsible="accordion">
            @if(\Illuminate\Support\Facades\Auth::user()->is_admin==1)
                <li><a href="{{route('Powner')}}" class="menu-active"><i class="fa fa-tachometer"
                                                                         aria-hidden="true"></i> Dashboard</a></li>
            @elseif(\Illuminate\Support\Facades\Auth::user()->is_admin==2)
                <li><a href="{{route('Howner')}}" class="menu-active"><i class="fa fa-tachometer"
                                                                         aria-hidden="true"></i> Dashboard</a></li>
            @elseif(\Illuminate\Support\Facades\Auth::user()->is_admin==3)
                <li><a href="{{route('userDashboard')}}" class="menu-active"><i class="fa fa-tachometer"
                                                                         aria-hidden="true"></i> Dashboard</a></li>
            @endif
            <li><a href="javascript:void(0)" class="collapsible-header"><i class="fa fa-list-ul" aria-hidden="true"></i>
                    Listing</a>
                <div class="collapsible-body left-sub-menu">
                    <ul>
                        <li><a href="{{route('listings.index')}}">All listing</a></li>
                        <li><a href="{{route('listings.create')}}">Add New listing</a></li>
                    </ul>
                </div>
            </li>
                @if(\Illuminate\Support\Facades\Auth::user()->is_admin==0)
            <li><a href="javascript:void(0)" class="collapsible-header"><i class="fa fa-th-list" aria-hidden="true"></i>
                    Categories</a>
                <div class="collapsible-body left-sub-menu">
                    <ul>
                        <li><a href="{{route('categories.index')}}">All Categories</a></li>
                        <li><a href="{{route('categories.create')}}">Add New Category</a></li>
                    </ul>
                </div>
            </li>
                    <li><a href="javascript:void(0)" class="collapsible-header"><i class="fa fa-buysellads"
                                                                                   aria-hidden="true"></i>Countries</a>
                        <div class="collapsible-body left-sub-menu">
                            <ul>
                                <li><a href="{{route('countries.index')}}">All Countries</a></li>
                                <li><a href="{{route('countries.create')}}">Add New Country</a></li>
                            </ul>
                        </div>
                    </li>
                    <li><a href="javascript:void(0)" class="collapsible-header"><i class="fa fa-buysellads"
                                                                                   aria-hidden="true"></i>Cities</a>
                        <div class="collapsible-body left-sub-menu">
                            <ul>
                                <li><a href="{{route('cities.index')}}">All Cities</a></li>
                                <li><a href="{{route('cities.create')}}">Add New City</a></li>
                            </ul>
                        </div>
                    </li>
                @endif

            <li><a href="#"><i class="fa fa-usd" aria-hidden="true"></i> Payments</a></li>
            <li><a href="#"><i class="fa fa-money" aria-hidden="true"></i> Earnings</a></li>
            <li><a href="javascript:void(0)" class="collapsible-header"><i class="fa fa-bell-o" aria-hidden="true"></i>Notifications</a>
                <div class="collapsible-body left-sub-menu">
                    <ul>
                        <li><a href="#">All Notifications</a></li>
                        <li><a href="#">User Notifications</a></li>
                        <li><a href="#">Push Notifications</a></li>
                    </ul>
                </div>
            </li>
            <li><a href="{{route('profile')}}" class="collapsible-header"><i class="fa fa-tags" aria-hidden="true"></i>Profile</a>
{{--                <div class="collapsible-body left-sub-menu">--}}
{{--                    <ul>--}}
{{--                        <li><a href="admin-price.html">All List Price</a></li>--}}
{{--                        <li><a href="admin-price-list.html">Add New Price</a></li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
            </li>
{{--            <li><a href="javascript:void(0)" class="collapsible-header"><i class="fa fa-rss" aria-hidden="true"></i>--}}
{{--                    Blog & Articals</a>--}}
{{--                <div class="collapsible-body left-sub-menu">--}}
{{--                    <ul>--}}
{{--                        <li><a href="admin-blog.html">All Blogs</a></li>--}}
{{--                        <li><a href="admin-blog-add.html">Add Blog</a></li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--            </li>--}}
{{--            <li><a href="admin-setting.html"><i class="fa fa-cogs" aria-hidden="true"></i> Setting</a></li>--}}
{{--            <li><a href="admin-social-media.html"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Social--}}
{{--                    Media</a></li>--}}
            <li><a href="{{ route('logout') }}"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-in" aria-hidden="true"></i> Logout</a></li>
                <form id="logout-form" method="POST" action="{{ route('logout') }}">
                    @csrf
                </form>
        </ul>
    </div>
</div>