@extends('frontend.master')
@section('content')

    {{--<div class="page-hero overlay overlay-blue no-overflow">--}}
    {{--    <div class="overlay-text d-flex align-items-center">--}}
    {{--        <div class="container">--}}
    {{--            <form action="{{route('search')}}" method="get" enctype="multipart/form-data" autocomplete="off">--}}
    {{--            <div class="row">--}}
    {{--                <div class="col-12">--}}
    {{--                    <div class="h1">Book your Hotel now</div>--}}
    {{--                    <p>Expolore best rated tours, hotels and restaurants around the world</p>--}}
    {{--                    <!-- /.main-home-search -->--}}
    {{--                </div>--}}

    {{--                <div class="main-home-search col-md-3">--}}
    {{--                    <div class="search-icon">--}}
    {{--                        <i class="fa fa-search"></i>--}}
    {{--                    </div>--}}
    {{--                    <!-- /.search-icon -->--}}
    {{--                    <input type="text" class="search-input test" placeholder='Try to find "Hotels" & "Tours"'/>--}}
    {{--                    <!-- /.search-input -->--}}
    {{--                    <div class="search-result-drop-down">--}}
    {{--                        <div class="result-list showList">--}}

    {{--                        </div>--}}
    {{--                        <!-- /.result-list -->--}}
    {{--                    </div>--}}
    {{--                    <!-- /.search-result-drop-down -->--}}
    {{--                </div>--}}
    {{--                <div class="main-home-search col-md-3">--}}
    {{--                    <input type="text" class="search-input checkin_checkout" name="datefilter" sugge value="" placeholder="Check in - Check out"/>--}}
    {{--                </div>--}}
    {{--                <div class="main-home-search col-md-3">--}}
    {{--                    <label style="color:grey;">Number of Guests</label><br>--}}
    {{--                    <select class="search-input guests" name="number_guests">--}}
    {{--                        <option value="1">1</option>--}}
    {{--                        <option value="2">2</option>--}}
    {{--                        <option value="3">3</option>--}}
    {{--                        <option value="4">4</option>--}}
    {{--                    </select>--}}
    {{--                </div>--}}
    {{--                <div class="">--}}
    {{--                        <button type="submit" id="filter" class="btn btn-book-now main-home-search col-md-3"><i class="fa fa-search" style="color: #fff3cd!important;"></i> Search</button>--}}
    {{--                </div>--}}
    {{--                <input type="hidden" class="location" name="location_id" value="">--}}

    {{--                <!-- /.col-12 -->--}}
    {{--            </div>--}}
    {{--            </form>--}}
    {{--            <!-- /.row -->--}}
    {{--        </div>--}}
    {{--        <!-- /.container -->--}}
    {{--    </div>--}}
    {{--    <!-- /.overlay-text -->--}}
    {{--    <div class="parallax-holder">--}}
    {{--        <img class="bg-parallax bg-parallax-neg" src="{{asset('assets/images/image11.jpg')}}')}}" data-z-index="1" data-parallax='{"y": -250,  "scale": 1.15, "smoothness": 15}' alt="image alt"/>--}}
    {{--    </div>--}}
    {{--    <!-- /.parallax-holder -->--}}
    {{--</div>--}}
    {{--<!-- /.page-hero -->--}}
    {{--<div class="section">--}}
    {{--    <div class="container">--}}
    {{--        <div class="row">--}}
    {{--            <div class="col-12 d-flex align-items-end justify-content-between">--}}
    {{--                <div class="flex-1">--}}
    {{--                    <h3>Top Destinations</h3>--}}
    {{--                    <div>Top destination subtitle</div>--}}
    {{--                </div>--}}
    {{--                <div class="text-right">--}}
    {{--                    <a href="#">See all</a>--}}
    {{--                </div>--}}
    {{--                <!-- /.float-right -->--}}
    {{--            </div>--}}
    {{--            <!-- /.col-12 -->--}}
    {{--        </div>--}}
    {{--        <!-- /.row -->--}}
    {{--        <div class="row">--}}
    {{--            <div class="col-12">--}}
    {{--                <div class="owl-carousel list-carousel">--}}
    {{--                    <div class="list-card">--}}
    {{--                        <div class="img">--}}
    {{--                            <img src="{{asset('assets/images/image7.jpg')}}')}}" alt="image alt"/>--}}
    {{--                        </div>--}}
    {{--                        <!-- /.img -->--}}
    {{--                        <div class="info">--}}
    {{--                            <div class="title">--}}
    {{--                                Place Title                                </div>--}}
    {{--                            <!-- /.title -->--}}
    {{--                            <div class="descrtiption">--}}
    {{--                                Short descrtiption text                                        <span class="review-star-rate small">--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    </span>--}}
    {{--                                <!-- /.review-starts-list small -->--}}
    {{--                            </div>--}}
    {{--                            <!-- /.descrtiption -->--}}
    {{--                        </div>--}}
    {{--                        <!-- /.info -->--}}
    {{--                    </div>--}}
    {{--                    <!-- /.list-card -->--}}
    {{--                    <div class="list-card">--}}
    {{--                        <div class="img">--}}
    {{--                            <img src="{{asset('assets/images/image6.jpg')}}')}}" alt="image alt"/>--}}
    {{--                        </div>--}}
    {{--                        <!-- /.img -->--}}
    {{--                        <div class="info">--}}
    {{--                            <div class="title">--}}
    {{--                                Place Title                                </div>--}}
    {{--                            <!-- /.title -->--}}
    {{--                            <div class="descrtiption">--}}
    {{--                                Short descrtiption text                                        <span class="review-star-rate small">--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    </span>--}}
    {{--                                <!-- /.review-starts-list small -->--}}
    {{--                            </div>--}}
    {{--                            <!-- /.descrtiption -->--}}
    {{--                        </div>--}}
    {{--                        <!-- /.info -->--}}
    {{--                    </div>--}}
    {{--                    <!-- /.list-card -->--}}
    {{--                    <div class="list-card">--}}
    {{--                        <div class="img">--}}
    {{--                            <img src="{{asset('assets/images/image8.jpg')}}')}}" alt="image alt"/>--}}
    {{--                        </div>--}}
    {{--                        <!-- /.img -->--}}
    {{--                        <div class="info">--}}
    {{--                            <div class="title">--}}
    {{--                                Place Title                                </div>--}}
    {{--                            <!-- /.title -->--}}
    {{--                            <div class="descrtiption">--}}
    {{--                                Short descrtiption text                                        <span class="review-star-rate small">--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    </span>--}}
    {{--                                <!-- /.review-starts-list small -->--}}
    {{--                            </div>--}}
    {{--                            <!-- /.descrtiption -->--}}
    {{--                        </div>--}}
    {{--                        <!-- /.info -->--}}
    {{--                    </div>--}}
    {{--                    <!-- /.list-card -->--}}
    {{--                    <div class="list-card">--}}
    {{--                        <div class="img">--}}
    {{--                            <img src="{{asset('assets/images/image9.jpg')}}')}}" alt="image alt"/>--}}
    {{--                        </div>--}}
    {{--                        <!-- /.img -->--}}
    {{--                        <div class="info">--}}
    {{--                            <div class="title">--}}
    {{--                                Place Title                                </div>--}}
    {{--                            <!-- /.title -->--}}
    {{--                            <div class="descrtiption">--}}
    {{--                                Short descrtiption text                                        <span class="review-star-rate small">--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    </span>--}}
    {{--                                <!-- /.review-starts-list small -->--}}
    {{--                            </div>--}}
    {{--                            <!-- /.descrtiption -->--}}
    {{--                        </div>--}}
    {{--                        <!-- /.info -->--}}
    {{--                    </div>--}}
    {{--                    <!-- /.list-card -->--}}
    {{--                    <div class="list-card">--}}
    {{--                        <div class="img">--}}
    {{--                            <img src="{{asset('assets/images/image10.jpg')}}')}}" alt="image alt"/>--}}
    {{--                        </div>--}}
    {{--                        <!-- /.img -->--}}
    {{--                        <div class="info">--}}
    {{--                            <div class="title">--}}
    {{--                                Place Title                                </div>--}}
    {{--                            <!-- /.title -->--}}
    {{--                            <div class="descrtiption">--}}
    {{--                                Short descrtiption text                                        <span class="review-star-rate small">--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    </span>--}}
    {{--                                <!-- /.review-starts-list small -->--}}
    {{--                            </div>--}}
    {{--                            <!-- /.descrtiption -->--}}
    {{--                        </div>--}}
    {{--                        <!-- /.info -->--}}
    {{--                    </div>--}}
    {{--                    <!-- /.list-card -->--}}
    {{--                    <div class="list-card">--}}
    {{--                        <div class="img">--}}
    {{--                            <img src="{{asset('assets/images/image5.jpg')}}')}}" alt="image alt"/>--}}
    {{--                        </div>--}}
    {{--                        <!-- /.img -->--}}
    {{--                        <div class="info">--}}
    {{--                            <div class="title">--}}
    {{--                                Place Title                                </div>--}}
    {{--                            <!-- /.title -->--}}
    {{--                            <div class="descrtiption">--}}
    {{--                                Short descrtiption text                                        <span class="review-star-rate small">--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    </span>--}}
    {{--                                <!-- /.review-starts-list small -->--}}
    {{--                            </div>--}}
    {{--                            <!-- /.descrtiption -->--}}
    {{--                        </div>--}}
    {{--                        <!-- /.info -->--}}
    {{--                    </div>--}}
    {{--                    <!-- /.list-card -->--}}
    {{--                </div>--}}
    {{--                <!-- /.list-card owl-carousel list-carousel -->--}}
    {{--            </div>--}}
    {{--            <!-- /.col-12 -->--}}
    {{--        </div>--}}
    {{--        <!-- /.row -->--}}
    {{--        <div class="row mt-4">--}}
    {{--            <div class="col-12 d-flex align-items-end justify-content-between">--}}
    {{--                <div class="flex-1">--}}
    {{--                    <h3>Discover New Cities</h3>--}}
    {{--                    <div>Discover new cities subtitle</div>--}}
    {{--                </div>--}}
    {{--                <div class="text-right">--}}
    {{--                    <a href="#">See all</a>--}}
    {{--                </div>--}}
    {{--                <!-- /.float-right -->--}}
    {{--            </div>--}}
    {{--            <!-- /.col-12 -->--}}
    {{--        </div>--}}
    {{--        <!-- /.row -->--}}
    {{--        <div class="row">--}}
    {{--            <div class="col-12">--}}
    {{--                <div class="owl-carousel list-carousel">--}}
    {{--                    <div class="list-card">--}}
    {{--                        <div class="img">--}}
    {{--                            <img src="{{asset('assets/images/image4.jpg')}}')}}" alt="image alt"/>--}}
    {{--                        </div>--}}
    {{--                        <!-- /.img -->--}}
    {{--                        <div class="info">--}}
    {{--                            <div class="title">--}}
    {{--                                Place Title                                </div>--}}
    {{--                            <!-- /.title -->--}}
    {{--                            <div class="descrtiption">--}}
    {{--                                Short descrtiption text                                        <span class="review-star-rate small">--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    </span>--}}
    {{--                                <!-- /.review-starts-list small -->--}}
    {{--                            </div>--}}
    {{--                            <!-- /.descrtiption -->--}}
    {{--                        </div>--}}
    {{--                        <!-- /.info -->--}}
    {{--                    </div>--}}
    {{--                    <!-- /.list-card -->--}}
    {{--                    <div class="list-card">--}}
    {{--                        <div class="img">--}}
    {{--                            <img src="{{asset('assets/images/image3.jpg')}}')}}" alt="image alt"/>--}}
    {{--                        </div>--}}
    {{--                        <!-- /.img -->--}}
    {{--                        <div class="info">--}}
    {{--                            <div class="title">--}}
    {{--                                Place Title                                </div>--}}
    {{--                            <!-- /.title -->--}}
    {{--                            <div class="descrtiption">--}}
    {{--                                Short descrtiption text                                        <span class="review-star-rate small">--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    </span>--}}
    {{--                                <!-- /.review-starts-list small -->--}}
    {{--                            </div>--}}
    {{--                            <!-- /.descrtiption -->--}}
    {{--                        </div>--}}
    {{--                        <!-- /.info -->--}}
    {{--                    </div>--}}
    {{--                    <!-- /.list-card -->--}}
    {{--                    <div class="list-card">--}}
    {{--                        <div class="img">--}}
    {{--                            <img src="{{asset('assets/images/image2.jpg')}}')}}" alt="image alt"/>--}}
    {{--                        </div>--}}
    {{--                        <!-- /.img -->--}}
    {{--                        <div class="info">--}}
    {{--                            <div class="title">--}}
    {{--                                Place Title                                </div>--}}
    {{--                            <!-- /.title -->--}}
    {{--                            <div class="descrtiption">--}}
    {{--                                Short descrtiption text                                        <span class="review-star-rate small">--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    </span>--}}
    {{--                                <!-- /.review-starts-list small -->--}}
    {{--                            </div>--}}
    {{--                            <!-- /.descrtiption -->--}}
    {{--                        </div>--}}
    {{--                        <!-- /.info -->--}}
    {{--                    </div>--}}
    {{--                    <!-- /.list-card -->--}}
    {{--                    <div class="list-card">--}}
    {{--                        <div class="img">--}}
    {{--                            <img src="{{asset('assets/images/image1.jpg')}}')}}" alt="image alt"/>--}}
    {{--                        </div>--}}
    {{--                        <!-- /.img -->--}}
    {{--                        <div class="info">--}}
    {{--                            <div class="title">--}}
    {{--                                Place Title                                </div>--}}
    {{--                            <!-- /.title -->--}}
    {{--                            <div class="descrtiption">--}}
    {{--                                Short descrtiption text                                        <span class="review-star-rate small">--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    </span>--}}
    {{--                                <!-- /.review-starts-list small -->--}}
    {{--                            </div>--}}
    {{--                            <!-- /.descrtiption -->--}}
    {{--                        </div>--}}
    {{--                        <!-- /.info -->--}}
    {{--                    </div>--}}
    {{--                    <!-- /.list-card -->--}}
    {{--                    <div class="list-card">--}}
    {{--                        <div class="img">--}}
    {{--                            <img src="http://via.placeholder.com/270x285" alt="image alt"/>--}}
    {{--                        </div>--}}
    {{--                        <!-- /.img -->--}}
    {{--                        <div class="info">--}}
    {{--                            <div class="title">--}}
    {{--                                Place Title                                </div>--}}
    {{--                            <!-- /.title -->--}}
    {{--                            <div class="descrtiption">--}}
    {{--                                Short descrtiption text                                        <span class="review-star-rate small">--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    </span>--}}
    {{--                                <!-- /.review-starts-list small -->--}}
    {{--                            </div>--}}
    {{--                            <!-- /.descrtiption -->--}}
    {{--                        </div>--}}
    {{--                        <!-- /.info -->--}}
    {{--                    </div>--}}
    {{--                    <!-- /.list-card -->--}}
    {{--                    <div class="list-card">--}}
    {{--                        <div class="img">--}}
    {{--                            <img src="http://via.placeholder.com/270x285" alt="image alt"/>--}}
    {{--                        </div>--}}
    {{--                        <!-- /.img -->--}}
    {{--                        <div class="info">--}}
    {{--                            <div class="title">--}}
    {{--                                Place Title                                </div>--}}
    {{--                            <!-- /.title -->--}}
    {{--                            <div class="descrtiption">--}}
    {{--                                Short descrtiption text                                        <span class="review-star-rate small">--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                    <!-- /.rate fa-star-o -->--}}
    {{--                    </span>--}}
    {{--                                <!-- /.review-starts-list small -->--}}
    {{--                            </div>--}}
    {{--                            <!-- /.descrtiption -->--}}
    {{--                        </div>--}}
    {{--                        <!-- /.info -->--}}
    {{--                    </div>--}}
    {{--                    <!-- /.list-card -->--}}
    {{--                </div>--}}
    {{--                <!-- /.list-card owl-carousel list-carousel -->--}}
    {{--            </div>--}}
    {{--            <!-- /.col-12 -->--}}
    {{--        </div>--}}
    {{--        <!-- /.row -->--}}
    {{--        <div class="row mt-4">--}}
    {{--            <div class="col-12 d-flex align-items-end justify-content-between">--}}
    {{--                <div class="flex-1">--}}
    {{--                    <h3>Most Popular Experiences</h3>--}}
    {{--                    <div>Explore a different way to travel</div>--}}
    {{--                </div>--}}
    {{--                <div class="text-right">--}}
    {{--                    <a href="#">See all</a>--}}
    {{--                </div>--}}
    {{--                <!-- /.float-right -->--}}
    {{--            </div>--}}
    {{--            <!-- /.col-12 -->--}}
    {{--        </div>--}}
    {{--        <!-- /.row -->--}}
    {{--        <div class="row">--}}
    {{--            <div class="col-12">--}}
    {{--                <div class="owl-carousel list-carousel-md">--}}
    {{--                    <div class="list-card">--}}
    {{--                        <div class="img">--}}
    {{--                            <img src="{{asset('assets/images/image12.jpg')}}')}}" alt="image alt"/>--}}
    {{--                        </div>--}}
    {{--                        <!-- /.img -->--}}
    {{--                        <div class="info">--}}
    {{--                            <div class="title">--}}
    {{--                                Place Title                                </div>--}}
    {{--                            <!-- /.title -->--}}
    {{--                            <span class="review-star-rate small">--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                <!-- /.rate fa-star-o -->--}}
    {{--                    </span>--}}
    {{--                            <!-- /.review-starts-list small -->--}}
    {{--                            <div class="descrtiption mt-1">--}}
    {{--                                Short descrtiption text about the experience                                </div>--}}
    {{--                            <!-- /.descrtiption -->--}}
    {{--                        </div>--}}
    {{--                        <!-- /.info -->--}}
    {{--                    </div>--}}
    {{--                    <!-- /.list-card -->--}}
    {{--                    <div class="list-card">--}}
    {{--                        <div class="img">--}}
    {{--                            <img src="{{asset('assets/images/image13.jpg')}}')}}" alt="image alt"/>--}}
    {{--                        </div>--}}
    {{--                        <!-- /.img -->--}}
    {{--                        <div class="info">--}}
    {{--                            <div class="title">--}}
    {{--                                Place Title                                </div>--}}
    {{--                            <!-- /.title -->--}}
    {{--                            <span class="review-star-rate small">--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                <!-- /.rate fa-star-o -->--}}
    {{--                            <span class="rate"><i class="fa fa-star-o"></i></span>--}}
    {{--                                <!-- /.rate fa-star-o -->--}}
    {{--            </span>--}}
    {{--                            <!-- /.review-starts-list small -->--}}
    {{--                            <div class="descrtiption mt-1">--}}
    {{--                                Short descrtiption text about the experience                                </div>--}}
    {{--                            <!-- /.descrtiption -->--}}
    {{--                        </div>--}}
    {{--                        <!-- /.info -->--}}
    {{--                    </div>--}}
    {{--                    <!-- /.list-card -->--}}
    {{--                    <div class="list-card">--}}
    {{--                        <div class="img">--}}
    {{--                            <img src="{{asset('assets/images/image14.jpg')}}')}}" alt="image alt"/>--}}
    {{--                        </div>--}}
    {{--                        <!-- /.img -->--}}
    {{--                        <div class="info">--}}
    {{--                            <div class="title">--}}
    {{--                                Place Title                                </div>--}}
    {{--                            <!-- /.title -->--}}
    {{--                            <span class="review-star-rate small">--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                <!-- /.rate fa-star-o -->--}}
    {{--                            <span class="rate"><i class="fa fa-star-o"></i></span>--}}
    {{--                                <!-- /.rate fa-star-o -->--}}
    {{--            </span>--}}
    {{--                            <!-- /.review-starts-list small -->--}}
    {{--                            <div class="descrtiption mt-1">--}}
    {{--                                Short descrtiption text about the experience                                </div>--}}
    {{--                            <!-- /.descrtiption -->--}}
    {{--                        </div>--}}
    {{--                        <!-- /.info -->--}}
    {{--                    </div>--}}
    {{--                    <!-- /.list-card -->--}}
    {{--                    <div class="list-card">--}}
    {{--                        <div class="img">--}}
    {{--                            <img src="http://via.placeholder.com/800x400" alt="image alt"/>--}}
    {{--                        </div>--}}
    {{--                        <!-- /.img -->--}}
    {{--                        <div class="info">--}}
    {{--                            <div class="title">--}}
    {{--                                Place Title                                </div>--}}
    {{--                            <!-- /.title -->--}}
    {{--                            <span class="review-star-rate small">--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                <!-- /.rate fa-star-o -->--}}
    {{--                    </span>--}}
    {{--                            <!-- /.review-starts-list small -->--}}
    {{--                            <div class="descrtiption mt-1">--}}
    {{--                                Short descrtiption text about the experience                                </div>--}}
    {{--                            <!-- /.descrtiption -->--}}
    {{--                        </div>--}}
    {{--                        <!-- /.info -->--}}
    {{--                    </div>--}}
    {{--                    <!-- /.list-card -->--}}
    {{--                    <div class="list-card">--}}
    {{--                        <div class="img">--}}
    {{--                            <img src="http://via.placeholder.com/800x400" alt="image alt"/>--}}
    {{--                        </div>--}}
    {{--                        <!-- /.img -->--}}
    {{--                        <div class="info">--}}
    {{--                            <div class="title">--}}
    {{--                                Place Title                                </div>--}}
    {{--                            <!-- /.title -->--}}
    {{--                            <span class="review-star-rate small">--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                <!-- /.rate fa-star-o -->--}}
    {{--                    </span>--}}
    {{--                            <!-- /.review-starts-list small -->--}}
    {{--                            <div class="descrtiption mt-1">--}}
    {{--                                Short descrtiption text about the experience                                </div>--}}
    {{--                            <!-- /.descrtiption -->--}}
    {{--                        </div>--}}
    {{--                        <!-- /.info -->--}}
    {{--                    </div>--}}
    {{--                    <!-- /.list-card -->--}}
    {{--                    <div class="list-card">--}}
    {{--                        <div class="img">--}}
    {{--                            <img src="http://via.placeholder.com/800x400" alt="image alt"/>--}}
    {{--                        </div>--}}
    {{--                        <!-- /.img -->--}}
    {{--                        <div class="info">--}}
    {{--                            <div class="title">--}}
    {{--                                Place Title                                </div>--}}
    {{--                            <!-- /.title -->--}}
    {{--                            <span class="review-star-rate small">--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                <!-- /.rate fa-star-o -->--}}
    {{--                    </span>--}}
    {{--                            <!-- /.review-starts-list small -->--}}
    {{--                            <div class="descrtiption mt-1">--}}
    {{--                                Short descrtiption text about the experience                                </div>--}}
    {{--                            <!-- /.descrtiption -->--}}
    {{--                        </div>--}}
    {{--                        <!-- /.info -->--}}
    {{--                    </div>--}}
    {{--                    <!-- /.list-card -->--}}
    {{--                </div>--}}
    {{--                <!-- /.list-card owl-carousel list-carousel -->--}}
    {{--            </div>--}}
    {{--            <!-- /.col-12 -->--}}
    {{--        </div>--}}
    {{--        <!-- /.row -->--}}
    {{--        <div class="row mt-4">--}}
    {{--            <div class="col-12 d-flex align-items-end justify-content-between">--}}
    {{--                <div class="flex-1">--}}
    {{--                    <h3>Experiences in Los Angeles</h3>--}}
    {{--                    <div>Cum doctus civibus efficiantur in imperdiet deterruisset</div>--}}
    {{--                </div>--}}
    {{--                <div class="text-right">--}}
    {{--                    <a href="#">See all</a>--}}
    {{--                </div>--}}
    {{--                <!-- /.float-right -->--}}
    {{--            </div>--}}
    {{--            <!-- /.col-12 -->--}}
    {{--        </div>--}}
    {{--        <!-- /.row -->--}}
    {{--        <div class="row">--}}
    {{--            <div class="col-12">--}}
    {{--                <div class="owl-carousel list-carousel-lg">--}}
    {{--                    <div class="list-card">--}}
    {{--                        <div class="img">--}}
    {{--                            <img src="{{asset('assets/images/losangeles1.jpg')}}')}}" alt="image alt"/>--}}
    {{--                        </div>--}}
    {{--                        <!-- /.img -->--}}
    {{--                        <div class="info">--}}
    {{--                            <div class="title">--}}
    {{--                                Place Title                                </div>--}}
    {{--                            <!-- /.title -->--}}
    {{--                            <div class="descrtiption">--}}
    {{--                                        <span class="review-star-rate small">--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                            <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                            <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                            <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                            <!-- /.rate fa-star-o -->--}}
    {{--                            <span class="rate"><i class="fa fa-star-o"></i></span>--}}
    {{--                                            <!-- /.rate fa-star-o -->--}}
    {{--            </span>--}}
    {{--                                <!-- /.review-starts-list small -->--}}
    {{--                                Short descrtiption text about the experience                                </div>--}}
    {{--                            <!-- /.descrtiption -->--}}
    {{--                        </div>--}}
    {{--                        <!-- /.info -->--}}
    {{--                    </div>--}}
    {{--                    <!-- /.list-card -->--}}
    {{--                    <div class="list-card">--}}
    {{--                        <div class="img">--}}
    {{--                            <img src="{{asset('assets/images/losangeles2.jpg')}}')}}" alt="image alt"/>--}}
    {{--                        </div>--}}
    {{--                        <!-- /.img -->--}}
    {{--                        <div class="info">--}}
    {{--                            <div class="title">--}}
    {{--                                Place Title                                </div>--}}
    {{--                            <!-- /.title -->--}}
    {{--                            <div class="descrtiption">--}}
    {{--                                        <span class="review-star-rate small">--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                            <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                            <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                            <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                            <!-- /.rate fa-star-o -->--}}
    {{--                            <span class="rate"><i class="fa fa-star-o"></i></span>--}}
    {{--                                            <!-- /.rate fa-star-o -->--}}
    {{--            </span>--}}
    {{--                                <!-- /.review-starts-list small -->--}}
    {{--                                Short descrtiption text about the experience                                </div>--}}
    {{--                            <!-- /.descrtiption -->--}}
    {{--                        </div>--}}
    {{--                        <!-- /.info -->--}}
    {{--                    </div>--}}
    {{--                    <!-- /.list-card -->--}}
    {{--                    <div class="list-card">--}}
    {{--                        <div class="img">--}}
    {{--                            <img src="{{asset('assets/images/losangeles3.jpg')}}')}}" alt="image alt"/>--}}
    {{--                        </div>--}}
    {{--                        <!-- /.img -->--}}
    {{--                        <div class="info">--}}
    {{--                            <div class="title">--}}
    {{--                                Place Title                                </div>--}}
    {{--                            <!-- /.title -->--}}
    {{--                            <div class="descrtiption">--}}
    {{--                                        <span class="review-star-rate small">--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                            <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                            <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                            <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                            <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                            <!-- /.rate fa-star-o -->--}}
    {{--                    </span>--}}
    {{--                                <!-- /.review-starts-list small -->--}}
    {{--                                Short descrtiption text about the experience                                </div>--}}
    {{--                            <!-- /.descrtiption -->--}}
    {{--                        </div>--}}
    {{--                        <!-- /.info -->--}}
    {{--                    </div>--}}
    {{--                    <!-- /.list-card -->--}}
    {{--                    <div class="list-card">--}}
    {{--                        <div class="img">--}}
    {{--                            <img src="http://via.placeholder.com/800x400" alt="image alt"/>--}}
    {{--                        </div>--}}
    {{--                        <!-- /.img -->--}}
    {{--                        <div class="info">--}}
    {{--                            <div class="title">--}}
    {{--                                Place Title                                </div>--}}
    {{--                            <!-- /.title -->--}}
    {{--                            <div class="descrtiption">--}}
    {{--                                        <span class="review-star-rate small">--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                            <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                            <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                            <!-- /.rate fa-star-o -->--}}
    {{--                    <span class="rate full"><i class="fa fa-star"></i></span>--}}
    {{--                                            <!-- /.rate fa-star-o -->--}}
    {{--                            <span class="rate"><i class="fa fa-star-o"></i></span>--}}
    {{--                                            <!-- /.rate fa-star-o -->--}}
    {{--            </span>--}}
    {{--                                <!-- /.review-starts-list small -->--}}
    {{--                                Short descrtiption text about the experience                                </div>--}}
    {{--                            <!-- /.descrtiption -->--}}
    {{--                        </div>--}}
    {{--                        <!-- /.info -->--}}
    {{--                    </div>--}}
    {{--                    <!-- /.list-card -->--}}
    {{--                </div>--}}
    {{--                <!-- /.list-card owl-carousel list-carousel -->--}}
    {{--            </div>--}}
    {{--            <!-- /.col-12 -->--}}
    {{--        </div>--}}
    {{--        <!-- /.row -->--}}
    {{--        <div class="row mt-4">--}}
    {{--            <div class="col-12 d-flex align-items-end justify-content-between">--}}
    {{--                <div>--}}
    {{--                    <h3>Top Stories</h3>--}}
    {{--                    <div>Most popular stories from our blog</div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--            <!-- /.col-12 -->--}}
    {{--        </div>--}}
    {{--        <div class="row mt-4">--}}
    {{--            <div class="col-lg-8 col-md-8 col-sm-12">--}}
    {{--                <img src="{{asset('assets/images/italy1.jpg')}}')}}" alt="img" class="img-fluid">--}}
    {{--                <!-- /.img-fluid -->--}}
    {{--            </div>--}}
    {{--            <!-- /.col-lg-8 col-md-8 col-sm-12 -->--}}
    {{--            <div class="col-lg-4 col-md-4 col-sm-12">--}}
    {{--                <div class="p-0 p-lg-4 p-md-4 mt-3 mt-lg-0 mt-md-0">--}}
    {{--                    <h3>Exploring Hidden Gems of Italy</h3>--}}
    {{--                    <p class="mt-4">--}}
    {{--                        Lorem ipsum dolor sit amet, consec tetur adipiscing elit, nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.--}}
    {{--                    </p>--}}
    {{--                    <!-- /.mt-4 -->--}}
    {{--                </div>--}}
    {{--                <!-- /.p-4 -->--}}
    {{--            </div>--}}
    {{--            <!-- /.col-lg-4 col-md-4 col-sm-12 -->--}}
    {{--        </div>--}}
    {{--        <!-- /.row mt-4 -->--}}
    {{--    </div>--}}
    {{--    <!-- /.container -->--}}
    {{--</div>--}}
    {{--<!-- /.section -->--}}
    {{--<div class="section img" data-image-src="{{asset('assets/images/image14.jpg')}}')}}">--}}
    {{--    <div class="container">--}}
    {{--        <div class="row">--}}
    {{--            <div class="offset-3 col-6">--}}
    {{--                <div class="text-center">--}}
    {{--                    <h3>Newsletter</h3>--}}
    {{--                    <p>Subscribe to our weekly Newsletter and stay tuned.</p>--}}
    {{--                    <form action="#" method="post" class="mt-4">--}}
    {{--                        <div class=" form-group">--}}
    {{--                            <input type="text" id="news-letter-email" name="news-letter-email" placeholder="youremail@email.com" class="form-control">--}}
    {{--                            <!-- /.form-control -->--}}
    {{--                        </div>--}}
    {{--                        <!-- /.form-group -->--}}
    {{--                        <input type="submit" value="Subscribe Now!" class="btn btn-large btn-outline-primary mt-3"/>--}}
    {{--                    </form>--}}
    {{--                </div>--}}
    {{--                <!-- /.text-center -->--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}
    {{--</div>--}}
    <!-- /.section -->
    <!--HOME PROJECTS-->
    <!--BANNER AND SERACH BOX-->
    <section class="dir3-home-head">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="dir-ho-tl">
                        <ul>
                            <li>
                                <a href="{{route('index')}}"><img src="{{asset('frontend/images/logo.png')}}" alt=""> </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="dir-ho-tr">
                        <ul>


                            @if(!\Illuminate\Support\Facades\Auth::user())
                                <li><a href="{{route('register')}}">Register</a></li>
                                <li><a href="{{route('login')}}">Sign In</a></li>
                            @else
                                @if(\Illuminate\Support\Facades\Auth::user()->is_admin==1)
                                    <li><a href="{{route('Howner')}}">Dashboard</a>
                                    </li>
                                @else
                                    <li><a href="{{route('Powner')}}">Dashboard</a>
                                    </li>
                                @endif
                                <li><a href="{{route('listings.create')}}"><i class="fa fa-plus" aria-hidden="true"></i>
                                        Add Listing</a>
                                </li>

                            @endif



                            @if(\Illuminate\Support\Facades\Auth::user())
                                <li>
                                    <a onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        <i class="fa fa-power-off mr-2"></i> Logout
                                    </a>
                                    <form id="logout-form" method="POST" action="{{ route('logout') }}">
                                        @csrf
                                    </form>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="container dir-ho-t-sp">
            <div class="row">
                <div class="dir-hr1 dir-cat-search">
                    <div class="dir-ho-t-tit">
                        <h1>Connect with the right<br>Service Experts</h1>
                        <p>Find B2B & B2C businesses contact addresses, phone numbers,<br> user ratings and reviews.</p>
                    </div>

                    <form action="{{route('search')}}" class="cate-search-form" method="get"
                          enctype="multipart/form-data" autocomplete="off">

                        <div class="input-field">
                            <input type="text" class="select-search1 test" value="">
                            <label for="select-search">Try to find "Hotels" & "Tours"</label>
                            {{--                            <ul class="autocomplete-content dropdown-content">--}}
                            <div class="showList"></div>
                        {{--                            </ul>--}}
                        <!-- /.search-result-drop-down -->
                        </div>


                        <div class="input-field">
                            <input type="text" id="select-city" class="autocomplete">
                            <label for="select-city">Enter city</label>
                        </div>
                        <div class="input-field">
                            <input type="text" name="category_name" id="category"
                                   class="autocomplete auto-category category">
                            <label>Select category</label>
                        </div>
                        <div class="input-field">
                            <input type="submit" value="search" class="waves-effect waves-light tourz-sear-btn"></div>
                        {{--                            <input type="hidden" name="category_name" id="category" value="">--}}
                    </form>
                </div>
            </div>
        </div>
    </section>


    <section>
        <div class="land-full land-packages">
            <div class="container">
                <div class="com-title">
                    <h2>Popular <span>Services</span></h2>
                    <p>Explore some of the best business from around the world from our partners and friends.</p>
                </div>
                <div class="land-pack">
                    <ul>
                        <li>
                            <div class="land-pack-grid">
                                <div class="land-pack-grid-img">
                                    <img src="{{asset('frontend/images/services/20.jpeg')}}" alt="">
                                </div>
                                <div class="land-pack-grid-text">
                                    <h4>Hotel Bookings</h4>
                                    <a href="service-booking.html" class="land-pack-grid-btn">Book Now</a></div>
                            </div>
                        </li>
                        <li>
                            <div class="land-pack-grid">
                                <div class="land-pack-grid-img">
                                    <img src="{{asset('frontend/images/services/p1.jpg')}}" alt="">
                                </div>
                                <div class="land-pack-grid-text">
                                    <h4>Real Estate</h4>
                                    <a href="service-booking.html" class="land-pack-grid-btn land-pack-grid-btn-blu">Book
                                        Now</a></div>
                            </div>
                        </li>
                        <li>
                            <div class="land-pack-grid">
                                <div class="land-pack-grid-img">
                                    <img src="{{asset('frontend/images/services/10.jpeg')}}" alt="">
                                </div>
                                <div class="land-pack-grid-text">
                                    <h4>Health Check-up</h4>
                                    <a href="service-booking.html" class="land-pack-grid-btn land-pack-grid-btn-yel">Book
                                        Now</a></div>
                            </div>
                        </li>
                        <li>
                            <div class="land-pack-grid">
                                <div class="land-pack-grid-img">
                                    <img src="{{asset('frontend/images/services/ser5.jpg')}}" alt="">
                                </div>
                                <div class="land-pack-grid-text">
                                    <h4>Cab Booking</h4>
                                    <a href="service-booking.html" class="land-pack-grid-btn land-pack-grid-btn-red">Book
                                        Now</a></div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--FIND YOUR SERVICE-->
    <section class="com-padd com-padd-redu-bot1 pad-bot-red-40">
        <div class="container">
            <div class="row">
                <div class="com-title">
                    <h2>Find your <span>Services</span></h2>
                    <p>Explore some of the best business from around the world from our partners and friends.</p>
                </div>
                <div class="dir-hli">
                    <ul>
                    @foreach($categories as $category)
                        <!--=====LISTINGS======-->
                            <li class="col-md-3 col-sm-6">
                                <a href="list.html">
                                    <div class="dir-hli-5">
                                        <div class="dir-hli-1">
                                            <div class="dir-hli-3"><img src="{{asset('frontend/images/hci1.png')}}"
                                                                        alt="">
                                            </div>
                                            <div class="dir-hli-4"></div>
                                            <img src="{{asset('frontend/images/services/15.jpg')}}" alt=""></div>
                                        <div class="dir-hli-2">
                                            <h4>{{$category->name}} <span class="dir-ho-cat">Show All ({{$category->where('category_id',$category->id)->count()}})</span>
                                            </h4></div>
                                    </div>
                                </a>
                            </li>
                        @endforeach

                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--EXPLORE CITY LISTING-->
    <section class="com-padd com-padd-redu-top">
        <div class="container">
            <div class="row">
                <div class="com-title">
                    <h2>Explore your <span>City Listings</span></h2>
                    <p>Explore some of the best business from around the world from our partners and friends.</p>
                </div>
                @foreach($categories as $key=>$category)
                    @if($key==0)
                        <div class="col-md-6">
                            <a href="list-lead.html">
                                <div class="list-mig-like-com">
                                    <div class="list-mig-lc-img">
                                        @if(sizeof($category->pictures)>0)
                                            @foreach($category->pictures as $picture)
                                                @if($picture->type==1)
                                                    <img src="{{asset('hotels/'.$picture->name)}}" alt=""/>
                                                    @break
                                                @endif
                                            @endforeach
                                        @else
                                            <img src="{{asset('hotel/$category->picture->name')}}"
                                                 alt=""/>
                                        @endif
                                    </div>
                                    <div class="list-mig-lc-con">
                                        <div class="list-rat-ch list-room-rati"><span>4.0</span> <i class="fa fa-star"
                                                                                                    aria-hidden="true"></i>
                                            <i
                                                    class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star"
                                                                                                  aria-hidden="true"></i>
                                            <i
                                                    class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-o"
                                                                                                  aria-hidden="true"></i>
                                        </div>
                                        <h5>{{$category->city->country->name}}</h5>
                                        <p>{{$category->city->where('country_id',$category->city->country->id)->count()}} Cities . {{$category->where('city_id',$category->city->id)->count()}} Listings . 3648 Users</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endif
                    <div class="col-md-3">
                        <a href="{{route('cityListing',$category->city->name)}}">
                            <div class="list-mig-like-com">
                                <div class="list-mig-lc-img">
                                    @if(sizeof($category->pictures)>0)
                                        @foreach($category->pictures as $picture)
                                            @if($picture->type==1)
                                                <img src="{{asset('hotels/'.$picture->name)}}" alt=""/>
                                                @break
                                            @endif
                                        @endforeach
                                    @else
                                        <img src="{{asset('frontend/images/listing/home2.jpg')}}"
                                             alt=""/>
                                    @endif
                                   </div>
                                <div class="list-mig-lc-con list-mig-lc-con2">
                                    <h5>{{$category->city->country->name}}</h5>
                                    <p>{{$category->city->where('country_id',$category->city->country->id)->count()}} Cities . {{$category->where('city_id',$category->city->id)->count()}} Listings . 3648 Users</p>
                                </div>
                            </div>
                        </a>
                    </div>
                        <div class="col-md-3">
                            <a href="list-lead.html">
                                <div class="list-mig-like-com">
                                    <div class="list-mig-lc-img">
                                        @if(sizeof($category->pictures)>0)
                                            @foreach($category->pictures as $picture)
                                                @if($picture->type==1)
                                                    <img src="{{asset('hotels/'.$picture->name)}}" alt=""/>
                                                    @break
                                                @endif
                                            @endforeach
                                        @else
                                            <img src="{{asset('frontend/images/listing/home2.jpg')}}"
                                                 alt=""/>
                                        @endif
                                    </div>
                                    <div class="list-mig-lc-con list-mig-lc-con2">
                                        <h5>{{$category->city->country->name}}</h5>
                                        <p>{{$category->city->where('country_id',$category->city->country->id)->count()}} Cities . {{$category->where('city_id',$category->city->id)->count()}} Listings . 3648 Users</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="list-lead.html">
                                <div class="list-mig-like-com">
                                    <div class="list-mig-lc-img">
                                        @if(sizeof($category->pictures)>0)
                                            @foreach($category->pictures as $picture)
                                                @if($picture->type==1)
                                                    <img src="{{asset('hotels/'.$picture->name)}}" alt=""/>
                                                    @break
                                                @endif
                                            @endforeach
                                        @else
                                            <img src="{{asset('frontend/images/listing/home2.jpg')}}"
                                                 alt=""/>
                                        @endif
                                    </div>
                                    <div class="list-mig-lc-con list-mig-lc-con2">
                                        <h5>{{$category->city->country->name}}</h5>
                                        <p>{{$category->city->where('country_id',$category->city->country->id)->count()}} Cities . {{$category->where('city_id',$category->city->id)->count()}} Listings . 3648 Users</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="list-lead.html">
                                <div class="list-mig-like-com">
                                    <div class="list-mig-lc-img">
                                        @if(sizeof($category->pictures)>0)
                                            @foreach($category->pictures as $picture)
                                                @if($picture->type==1)
                                                    <img src="{{asset('hotels/'.$picture->name)}}" alt=""/>
                                                    @break
                                                @endif
                                            @endforeach
                                        @else
                                            <img src="{{asset('frontend/images/listing/home2.jpg')}}"
                                                 alt=""/>
                                        @endif
                                    </div>
                                    <div class="list-mig-lc-con list-mig-lc-con2">
                                        <h5>{{$category->city->country->name}}</h5>
                                        <p>{{$category->city->where('country_id',$category->city->country->id)->count()}} Cities . {{$category->where('city_id',$category->city->id)->count()}} Listings . 3648 Users</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        @break
                @endforeach
            </div>
        </div>
    </section>
    <!--ADD BUSINESS-->
    <section class="com-padd quic-book-ser-full">
        <div class="quic-book-ser">
            <div class="quic-book-ser-inn">
                <div class="quic-book-ser-left">
                    <div class="land-com-form">
                        <h2>Quick service request</h2>
                        <form>
                            <ul>
                                <li>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <input type="text" class="validate" required>
                                            <label>Name</label>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <input type="number" class="validate" required>
                                            <label>Phone number</label>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <input type="email" class="validate" required>
                                            <label>Email id</label>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <input type="text" id="select-category1" class="autocomplete auto-category">
                                            <label for="select-category1">Select your Service</label>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <input type="submit" value="Send Request">
                                        </div>
                                    </div>
                                </li>
                                <li><p>Praesent felis velit, maximus at dapibus semper, fermentum sagittis diam. <a
                                                href="#">Privacy Policy</a></p></li>
                            </ul>
                        </form>
                    </div>
                </div>
                <div class="quic-book-ser-right">
                    <div class="hom-cre-acc-left">
                        <h3>What service do you need? <span>Business Directory</span></h3>
                        <p>Tell us more about your requirements so that we can connect you to the right service
                            provider.</p>
                        <ul>
                            <li><img src="{{asset('frontend/images/icon/7.png')}}" alt="">
                                <div>
                                    <h5>Tell us more about your requirements</h5>
                                    <p>Imagine you have made your presence online through a local online directory, but
                                        your competitors have..</p>
                                </div>
                            </li>
                            <li><img src="{{asset('frontend/images/icon/5.png')}}" alt="">
                                <div>
                                    <h5>We connect with right service provider</h5>
                                    <p>Advertising your business to area specific has many advantages. For local
                                        businessmen, it is an opportunity..</p>
                                </div>
                            </li>
                            <li><img src="{{asset('frontend/images/icon/6.png')}}" alt="">
                                <div>
                                    <h5>Happy with our service</h5>
                                    <p>Your local business too needs brand management and image making. As you know the
                                        local market..</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!--BEST THINGS-->
    <section class="com-padd com-padd-redu-bot">
        <div class="container dir-hom-pre-tit">
            <div class="row">
                <div class="com-title">
                    <h2>Top Trendings for <span>your City</span></h2>
                    <p>Explore some of the best tips from around the world from our partners and friends.</p>
                </div>
                <div class="col-md-6">
                    <div>
                        <!--POPULAR LISTINGS-->
                        <div class="home-list-pop">
                            <!--POPULAR LISTINGS IMAGE-->
                            <div class="col-md-3"><img src="{{asset('frontend/images/services/tr1.jpg')}}" alt=""/>
                            </div>
                            <!--POPULAR LISTINGS: CONTENT-->
                            <div class="col-md-9 home-list-pop-desc"><a href="automobile-listing-details.html"><h3>
                                        Import Motor America</h3></a>
                                <h4>Express Avenue Mall, Santa Monica</h4>
                                <p>28800 Orchard Lake Road, Suite 180 Farmington Hills, U.S.A.</p> <span
                                        class="home-list-pop-rat">4.2</span>
                                <div class="hom-list-share">
                                    <ul>
                                        <li><a href="#!"><i class="fa fa-bar-chart" aria-hidden="true"></i> 52</a></li>
                                        <li><a href="#!"><i class="fa fa-heart-o" aria-hidden="true"></i> 32</a></li>
                                        <li><a href="#!"><i class="fa fa-eye" aria-hidden="true"></i> 420</a></li>
                                        <li><a href="#!"><i class="fa fa-share-alt" aria-hidden="true"></i> 570</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--POPULAR LISTINGS-->
                        <div class="home-list-pop">
                            <!--POPULAR LISTINGS IMAGE-->
                            <div class="col-md-3"><img src="{{asset('frontend/images/services/tr2.jpg')}}" alt=""/>
                            </div>
                            <!--POPULAR LISTINGS: CONTENT-->
                            <div class="col-md-9 home-list-pop-desc"><a href="property-listing-details.html"><h3>Luxury
                                        Property</h3></a>
                                <h4>Express Avenue Mall, New York</h4>
                                <p>28800 Orchard Lake Road, Suite 180 Farmington Hills, U.S.A.</p> <span
                                        class="home-list-pop-rat">4.2</span>
                                <div class="hom-list-share">
                                    <ul>
                                        <li><a href="#!"><i class="fa fa-bar-chart" aria-hidden="true"></i> 52</a></li>
                                        <li><a href="#!"><i class="fa fa-heart-o" aria-hidden="true"></i> 32</a></li>
                                        <li><a href="#!"><i class="fa fa-eye" aria-hidden="true"></i> 420</a></li>
                                        <li><a href="#!"><i class="fa fa-share-alt" aria-hidden="true"></i> 570</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--POPULAR LISTINGS-->
                        <div class="home-list-pop">
                            <!--POPULAR LISTINGS IMAGE-->
                            <div class="col-md-3"><img src="{{asset('frontend/images/services/tr3.jpg')}}" alt=""/>
                            </div>
                            <!--POPULAR LISTINGS: CONTENT-->
                            <div class="col-md-9 home-list-pop-desc"><a href="shop-listing-details.html"><h3>Spicy
                                        Supermarket Shop</h3></a>
                                <h4>Express Avenue Mall, Chicago</h4>
                                <p>28800 Orchard Lake Road, Suite 180 Farmington Hills, U.S.A.</p> <span
                                        class="home-list-pop-rat">4.2</span>
                                <div class="hom-list-share">
                                    <ul>
                                        <li><a href="#!"><i class="fa fa-bar-chart" aria-hidden="true"></i> 52</a></li>
                                        <li><a href="#!"><i class="fa fa-heart-o" aria-hidden="true"></i> 32</a></li>
                                        <li><a href="#!"><i class="fa fa-eye" aria-hidden="true"></i> 420</a></li>
                                        <li><a href="#!"><i class="fa fa-share-alt" aria-hidden="true"></i> 570</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--POPULAR LISTINGS-->
                        <div class="home-list-pop">
                            <!--POPULAR LISTINGS IMAGE-->
                            <div class="col-md-3"><img src="{{asset('frontend/images/services/s4.jpeg')}}" alt=""/>
                            </div>
                            <!--POPULAR LISTINGS: CONTENT-->
                            <div class="col-md-9 home-list-pop-desc"><a href="list-lead.html"><h3>Packers and
                                        Movers</h3></a>
                                <h4>Express Avenue Mall, Toronto</h4>
                                <p>28800 Orchard Lake Road, Suite 180 Farmington Hills, U.S.A.</p> <span
                                        class="home-list-pop-rat">4.2</span>
                                <div class="hom-list-share">
                                    <ul>
                                        <li><a href="#!"><i class="fa fa-bar-chart" aria-hidden="true"></i> 52</a></li>
                                        <li><a href="#!"><i class="fa fa-heart-o" aria-hidden="true"></i> 32</a></li>
                                        <li><a href="#!"><i class="fa fa-eye" aria-hidden="true"></i> 420</a></li>
                                        <li><a href="#!"><i class="fa fa-share-alt" aria-hidden="true"></i> 570</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div>
                        <!--POPULAR LISTINGS-->
                        <div class="home-list-pop">
                            <!--POPULAR LISTINGS IMAGE-->
                            <div class="col-md-3"><img src="{{asset('frontend/images/services/s5.jpeg')}}" alt=""/>
                            </div>
                            <!--POPULAR LISTINGS: CONTENT-->
                            <div class="col-md-9 home-list-pop-desc"><a href="list-lead.html"><h3>Tour and Travels</h3>
                                </a>
                                <h4>Express Avenue Mall, Los Angeles</h4>
                                <p>28800 Orchard Lake Road, Suite 180 Farmington Hills, U.S.A.</p> <span
                                        class="home-list-pop-rat">4.2</span>
                                <div class="hom-list-share">
                                    <ul>
                                        <li><a href="#!"><i class="fa fa-bar-chart" aria-hidden="true"></i> 52</a></li>
                                        <li><a href="#!"><i class="fa fa-heart-o" aria-hidden="true"></i> 32</a></li>
                                        <li><a href="#!"><i class="fa fa-eye" aria-hidden="true"></i> 420</a></li>
                                        <li><a href="#!"><i class="fa fa-share-alt" aria-hidden="true"></i> 570</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--POPULAR LISTINGS-->
                        <div class="home-list-pop">
                            <!--POPULAR LISTINGS IMAGE-->
                            <div class="col-md-3"><img src="{{asset('frontend/images/services/s6.jpeg')}}" alt=""/>
                            </div>
                            <!--POPULAR LISTINGS: CONTENT-->
                            <div class="col-md-9 home-list-pop-desc"><a href="list-lead.html"><h3>Andru Modular
                                        Kitchen</h3></a>
                                <h4>Express Avenue Mall, San Diego</h4>
                                <p>28800 Orchard Lake Road, Suite 180 Farmington Hills, U.S.A.</p> <span
                                        class="home-list-pop-rat">4.2</span>
                                <div class="hom-list-share">
                                    <ul>
                                        <li><a href="#!"><i class="fa fa-bar-chart" aria-hidden="true"></i> 52</a></li>
                                        <li><a href="#!"><i class="fa fa-heart-o" aria-hidden="true"></i> 32</a></li>
                                        <li><a href="#!"><i class="fa fa-eye" aria-hidden="true"></i> 420</a></li>
                                        <li><a href="#!"><i class="fa fa-share-alt" aria-hidden="true"></i> 570</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--POPULAR LISTINGS-->
                        <div class="home-list-pop">
                            <!--POPULAR LISTINGS IMAGE-->
                            <div class="col-md-3"><img src="{{asset('frontend/images/services/s7.jpeg')}}" alt=""/>
                            </div>
                            <!--POPULAR LISTINGS: CONTENT-->
                            <div class="col-md-9 home-list-pop-desc"><a href="list-lead.html"><h3>Rute Skin Care &
                                        Treatment</h3></a>
                                <h4>Express Avenue Mall, Toronto</h4>
                                <p>28800 Orchard Lake Road, Suite 180 Farmington Hills, U.S.A.</p> <span
                                        class="home-list-pop-rat">4.2</span>
                                <div class="hom-list-share">
                                    <ul>
                                        <li><a href="#!"><i class="fa fa-bar-chart" aria-hidden="true"></i> 52</a></li>
                                        <li><a href="#!"><i class="fa fa-heart-o" aria-hidden="true"></i> 32</a></li>
                                        <li><a href="#!"><i class="fa fa-eye" aria-hidden="true"></i> 420</a></li>
                                        <li><a href="#!"><i class="fa fa-share-alt" aria-hidden="true"></i> 570</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--POPULAR LISTINGS-->
                        <div class="home-list-pop mar-bot-red-0">
                            <!--POPULAR LISTINGS IMAGE-->
                            <div class="col-md-3"><img src="{{asset('frontend/images/services/s8.jpg')}}" alt=""/></div>
                            <!--POPULAR LISTINGS: CONTENT-->
                            <div class="col-md-9 home-list-pop-desc"><a href="list-lead.html"><h3>Health and
                                        Fitness</h3></a>
                                <h4>Express Avenue Mall, San Diego</h4>
                                <p>28800 Orchard Lake Road, Suite 180 Farmington Hills, U.S.A.</p> <span
                                        class="home-list-pop-rat">4.2</span>
                                <div class="hom-list-share">
                                    <ul>
                                        <li><a href="#!"><i class="fa fa-bar-chart" aria-hidden="true"></i> 52</a></li>
                                        <li><a href="#!"><i class="fa fa-heart-o" aria-hidden="true"></i> 32</a></li>
                                        <li><a href="#!"><i class="fa fa-eye" aria-hidden="true"></i> 420</a></li>
                                        <li><a href="#!"><i class="fa fa-share-alt" aria-hidden="true"></i> 570</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('page_level_scripts')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>

    <script type="text/javascript">
        $(function () {

            $('input[name="datefilter"]').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear'
                }
            });

            $('input[name="datefilter"]').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
            });

            $('input[name="datefilter"]').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

        });
    </script>
    <script>
        $(document).ready(function () {
            $('.test').keydown(function () {

                var key = $(this).val();

                $.ajax({
                    url: '{{route('ajaxSearch')}}',
                    type: 'get',
                    data: {
                        keyword: key
                    },

                    success: function (data) {
                        console.log(data);
                        $('.showList').html(data);
                        $('.titl1').click(function (e) {
                            e.preventDefault();
                            $('.test').val($('.title').html());
                            $('#category').val($('.titl1').data('category_name'));
                            $(".showList").remove();
                        })
                    }
                })
            })
        })
    </script>
@endsection
