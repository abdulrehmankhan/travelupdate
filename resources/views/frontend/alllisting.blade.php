@extends('frontend.master')
@section('content')
    <section class="dir-pa-sp-top">
        <div class="container com-padd dir-hom-pre-tit">
            <div class="com-title">
                <h2>New Businesses in<span> this month</span></h2>
                <p>Explore some of the best tips from around the world from our partners and friends.</p>
            </div>
            <div class="row span-none">
               @foreach($listings as $listing)
                    <div class="col-md-4">
                        <a href="#!">
                            <div class="list-mig-like-com com-mar-bot-30">
                                <div class="list-mig-lc-img">
                                    @if(sizeof($listing->pictures)>0)
                                    @foreach($listing->pictures as $picture)
                                        @if($picture->type==1)
                                           <img src="{{asset('hotels/'.$picture->name)}}" width="350" height="350">
                                        @endif
                                    @endforeach
                                    @else
                                        <img src="{{asset('frontend/images/9new.jpg')}}" alt="" width="350" height="350">
                                    @endif
                                    <span class="home-list-pop-rat list-mi-pr">${{$listing->price}}</span> </div>
                                <div class="list-mig-lc-con">
                                    <div class="list-rat-ch list-room-rati"> <span>4.0</span> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i> </div>
                                    <h5>{{$listing->name}}</h5>
                                    <p>{{$listing->city->name}}, {{$listing->city->country->name}}</p>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach



            </div>
            <br>
            <div class="row">
                <ul class="pagination list-pagenat">
                    <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a> </li>
                    <li class="active"><a href="#!">1</a> </li>
                    <li class="waves-effect"><a href="#!">2</a> </li>
                    <li class="waves-effect"><a href="#!">3</a> </li>
                    <li class="waves-effect"><a href="#!">4</a> </li>
                    <li class="waves-effect"><a href="#!">5</a> </li>
                    <li class="waves-effect"><a href="#!">6</a> </li>
                    <li class="waves-effect"><a href="#!">7</a> </li>
                    <li class="waves-effect"><a href="#!">8</a> </li>
                    <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a> </li>
                </ul>
            </div>
        </div>
    </section>

@endsection