@foreach($results as $key=>$result)
    <div class="search-result-item">
        <a href="#" class="titl1" data-category_name="{{$result->category->name}}">
            <div>
                <i class="flaticon-map-marker mr-2 small-icon"></i>
            </div>
            <div class="title">
                {{$result->name}}, {{$result->category->locations[$key]->name}},
            </div>
        </a>
        <!-- /.title -->
    </div>
@endforeach