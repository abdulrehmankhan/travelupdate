<header>

    <nav class="navbar navbar-light navbar-expand-md py-md-2 fixed-top">
        <div class="container">
            <a href="{{route('index')}}" class="navbar-brand">
                <img src="{{asset('assets/images/logo.png')}}" height="30" alt="logo" class="brand-logo"/>
            </a>
            <!-- /.navbar-brand -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                <!-- /.navbar-toggler-icon -->
            </button>
            <!-- /.navbar-toggler -->
            <div class="navbar-collapse collapse justify-content-end" id="navbarNav">
                <ul class="navbar-nav">

                    @if(!\Illuminate\Support\Facades\Auth::user())
                        <li class="nav-item  py-md-2">
                            <a href="{{route('login')}}" class="nav-link ">Login</a>
                            <!-- /.nav-link -->
                        </li>
                        <li class="nav-item  py-md-2">
                            <a href="{{route('register')}}" class="nav-link ">Signup</a>
                            <!-- /.nav-link -->
                        </li>
                @endif
                <!-- /.nav-item py-md-2 -->
                        @if(\Illuminate\Support\Facades\Auth::user())
                        <li class="nav-item  py-md-2">
                            <a href="{{route('index')}}" class="nav-link ">Home</a>
                            <!-- /.nav-link -->
                        </li>
                    <li class="nav-item dropdown py-md-2">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-haspopup="true" aria-expanded="true">Hotels</a>
                        <!-- /.nav-link -->
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="hotel-list-sidebar-left.html">Hotel List left sidebar</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href="hotel-list-sidebar-right.html">Hotel List right sidebar</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href="hotel-list-full-width.html">Hotel List Full Width</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href="hotel-details.html">Hotel Detail</a>
                            <!-- /.dropdown-item -->
                        </div>
                        <!-- /.dropdown-menu -->
                    </li>
                    <!-- /.nav-item py-md-2 -->
                    <li class="nav-item dropdown py-md-2">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-haspopup="true" aria-expanded="true">Pages</a>
                        <!-- /.nav-link -->
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#login-modal">Login</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href="#" data-toggle="modal"
                               data-target="#register-modal">Register</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#reset-password-modal">Reset
                                Password</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href="profile.html">Profile</a>

                            <a class="dropdown-item" href="profile-edit.html">Profile Edit</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href="profile-booking-history.html">Profile Booking History</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href="profile-payments.html">Profile Payments</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href="success-payment.html">Success Payment</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href="coming-soon.html">Coming Soon</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href="404.html">404 Page</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href="about.html">About</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href="contact-us.html">Contact us</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href="faq.html">FAQ</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href="privacy.html">Privacy & Policy</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href="terms.html">Terms and Conditions</a>
                            <!-- /.dropdown-item -->
                        </div>
                        <!-- /.dropdown-menu -->
                    </li>
                    <!-- /.nav-item py-md-2 -->
                    <li class="nav-item dropdown py-md-2">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-haspopup="true" aria-expanded="true">Blog</a>
                        <!-- /.nav-link -->
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="blog.html">Blog</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href="blog-single-post-full-width.html">Single Post Full Width</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href="blog-single-post-sidebar-left.html">Single Post Side Bar
                                Left</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href="blog-single-post-sidebar-right.html">Single Post Side Bar
                                Right</a>
                            <!-- /.dropdown-item -->
                        </div>
                        <!-- /.dropdown-menu -->
                    </li>
                    <!-- /.nav-item py-md-2 -->
                    <li class="nav-item dropdown py-md-2">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-haspopup="true" aria-expanded="true">USD</a>
                        <!-- /.nav-link -->
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#"><span class="font-weight-bold">USD</span> - United States
                                Dollar</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href="#"><span class="font-weight-bold">AED</span> - Emirati Dirham</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href="#"><span class="font-weight-bold">AUD</span> - Australian
                                Dollar</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href="#"><span class="font-weight-bold">AZN</span> - Azerbaijani
                                Manat</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href="#"><span class="font-weight-bold">BGN</span> - Bulgarian Lev</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href="#"><span class="font-weight-bold">BHD</span> - Bahraini Dinar</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href="#"><span class="font-weight-bold">CHF</span> - Swiss
                                Franc</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href="#"><span class="font-weight-bold">GBP</span> - British Pound</a>
                            <!-- /.dropdown-item -->
                        </div>
                        <!-- /.dropdown-menu -->
                    </li>
                    <!-- /.nav-item py-md-2 -->
                    <li class="nav-item dropdown py-md-2">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-haspopup="true" aria-expanded="true"><span
                                    class="flag-icon flag-icon-us flag-icon-squared mr-1"></span> Eng</a>
                        <!-- /.nav-link -->
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href=""><span
                                        class="flag-icon flag-icon-us flag-icon-squared mr-2"></span> English</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href=""><span
                                        class="flag-icon flag-icon-fr flag-icon-squared mr-2"></span> Français</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href=""><span
                                        class="flag-icon flag-icon-sa flag-icon-squared mr-2"></span> عربية</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href=""><span
                                        class="flag-icon flag-icon-tr flag-icon-squared mr-2"></span> Türkçe</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href=""><span
                                        class="flag-icon flag-icon-it flag-icon-squared mr-2"></span> Italiano</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href=""><span
                                        class="flag-icon flag-icon-es flag-icon-squared mr-2"></span> Español</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href=""><span
                                        class="flag-icon flag-icon-de flag-icon-squared mr-2"></span> Deutsch</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href=""><span
                                        class="flag-icon flag-icon-ru flag-icon-squared mr-2"></span> Русский</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href=""><span
                                        class="flag-icon flag-icon-pl flag-icon-squared mr-2"></span> Polski</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href=""><span
                                        class="flag-icon flag-icon-sv flag-icon-squared mr-2"></span> Svenska</a>
                            <!-- /.dropdown-item -->
                        </div>
                        <!-- /.dropdown-menu -->
                    </li>
                    <!-- /.nav-item py-md-2 -->
                    <li class="nav-item dropdown py-md-2">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-haspopup="true" aria-expanded="true">Renee Turner <img
                                    src="http://via.placeholder.com/90x90" alt="user profile img"
                                    class="user-profile-img"/></a>
                        <!-- /.nav-link -->
                        <div class="dropdown-menu">
                            <!-- /.dropdown-item -->
                            @if(\Illuminate\Support\Facades\Auth::user() && \Illuminate\Support\Facades\Auth::user()->is_admin==1)
                                <a class="dropdown-item" href="{{route('Howner')}}">Dashboard</a>
                            @elseif(\Illuminate\Support\Facades\Auth::user() && \Illuminate\Support\Facades\Auth::user()->is_admin==2)
                                <a class="dropdown-item" href="{{route('Powner')}}">Dashboard</a>
                            @endif
                            <a class="dropdown-item" href="profile-edit.html">Edit Profile</a>
                            <!-- /.dropdown-item -->
                            <a class="dropdown-item" href="#">Invite Friends</a>
                            <!-- /.dropdown-item -->
                            <form method="POST" action="{{ route('logout') }}">
                                @csrf

                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 this.closest('form').submit();" class="dropdown-item">
                                    Logout
                                </a>
                            </form>
                            <!-- /.dropdown-item -->
                        </div>
                        <!-- /.dropdown-menu -->
                    </li>
                    <!-- /.nav-item py-md-2 -->
                            @endif
                </ul>
                <!-- /.navbar-nav -->
            </div>
            <!-- /#navbarNav.navbar-collapse collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <!-- /.navbar navbar-dark bg-primary navbar-expand-md py-md-2 -->
</header>
