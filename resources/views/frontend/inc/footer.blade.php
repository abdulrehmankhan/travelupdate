<footer>
    <div class="container md">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12">
                <div class="btn-group btn-group-sm ">
                    <button type="button" class="btn btn-default btn-outline-secondary  no-round  dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        English        </button>
                    <!-- /.btn btn-danger dropdown-toggle -->
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href=""><span class="flag-icon flag-icon-us flag-icon-squared mr-2"></span> English</a>
                        <!-- /.dropdown-item -->
                        <a class="dropdown-item" href=""><span class="flag-icon flag-icon-fr flag-icon-squared mr-2"></span> Français</a>
                        <!-- /.dropdown-item -->
                        <a class="dropdown-item" href=""><span class="flag-icon flag-icon-sa flag-icon-squared mr-2"></span> عربية</a>
                        <!-- /.dropdown-item -->
                        <a class="dropdown-item" href=""><span class="flag-icon flag-icon-tr flag-icon-squared mr-2"></span> Türkçe</a>
                        <!-- /.dropdown-item -->
                        <a class="dropdown-item" href=""><span class="flag-icon flag-icon-it flag-icon-squared mr-2"></span> Italiano</a>
                        <!-- /.dropdown-item -->
                        <a class="dropdown-item" href=""><span class="flag-icon flag-icon-es flag-icon-squared mr-2"></span> Español</a>
                        <!-- /.dropdown-item -->
                        <a class="dropdown-item" href=""><span class="flag-icon flag-icon-de flag-icon-squared mr-2"></span> Deutsch</a>
                        <!-- /.dropdown-item -->
                        <a class="dropdown-item" href=""><span class="flag-icon flag-icon-ru flag-icon-squared mr-2"></span> Русский</a>
                        <!-- /.dropdown-item -->
                        <a class="dropdown-item" href=""><span class="flag-icon flag-icon-pl flag-icon-squared mr-2"></span> Polski</a>
                        <!-- /.dropdown-item -->
                        <a class="dropdown-item" href=""><span class="flag-icon flag-icon-sv flag-icon-squared mr-2"></span> Svenska</a>
                        <!-- /.dropdown-item -->
                    </div>
                    <!-- /.dropdown-menu  -->
                </div>
                <!-- /.btn-group -->
                <div class="btn-group btn-group-sm mt-2 d-block mb-4">
                    <button type="button" class="btn btn-default btn-outline-secondary  no-round  dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        United States Dollar        </button>
                    <!-- /.btn btn-danger dropdown-toggle -->
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#"><span class="font-weight-bold">USD</span> - United States Dollar</a>
                        <!-- /.dropdown-item -->
                        <a class="dropdown-item" href="#"><span class="font-weight-bold">AED</span> - Emirati Dirham</a>
                        <!-- /.dropdown-item -->
                        <a class="dropdown-item" href="#"><span class="font-weight-bold">AUD</span> - Australian Dollar</a>
                        <!-- /.dropdown-item -->
                        <a class="dropdown-item" href="#"><span class="font-weight-bold">AZN</span> - Azerbaijani Manat</a>
                        <!-- /.dropdown-item -->
                        <a class="dropdown-item" href="#"><span class="font-weight-bold">BGN</span> - Bulgarian Lev</a>
                        <!-- /.dropdown-item -->
                        <a class="dropdown-item" href="#"><span class="font-weight-bold">BHD</span> - Bahraini Dinar</a>
                        <!-- /.dropdown-item -->
                        <a class="dropdown-item" href="#"><span class="font-weight-bold">CHF</span> - Swiss Franc</a>
                        <!-- /.dropdown-item -->
                        <a class="dropdown-item" href="#"><span class="font-weight-bold">GBP</span> - British Pound</a>
                        <!-- /.dropdown-item -->
                    </div>
                    <!-- /.dropdown-menu mt-2 d-block mb-4 -->
                </div>
                <!-- /.btn-group -->


                <div class="small mb-1">Stay Tuned</div>
                <ul class="social-network">
                    <li>
                        <a href="#"><i class="fa fa-facebook"></i></a>
                    </li>
                    <!-- /li -->
                    <li>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                    </li>
                    <!-- /li -->
                    <li>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                    </li>
                    <!-- /li -->
                    <li>
                        <a href="#"><i class="fa fa-google-plus"></i></a>
                    </li>
                    <!-- /li -->
                    <li>
                        <a href="#"><i class="fa fa-youtube-play"></i></a>
                    </li>
                    <!-- /li -->
                    <li>
                        <a href="#"><i class="fa fa-pinterest-p"></i></a>
                    </li>
                    <!-- /li -->
                </ul>
                <!-- /.social-network -->

                <div class="small mb-2 mt-2">
                    Reviews by
                </div>
                <!-- /.small mb-1 -->
                <img src="{{asset('assets/icons/TA_logo_primary.svg')}}" alt="" height="25">

            </div>
            <!-- /.col-lg-3 col-md-3 col-sm-12 -->
            <div class="col-lg-3 col-md-3 col-sm-12">
                <div class="footer-title mt-4 mt-lg-0 mt-md-0">
                    J Travels
                </div>
                <!-- /.footer-title -->
                <ul class="footer-nav">
                    <li class="footer-nav-item">
                        <a href="terms-conditions.html" class="footer-link">Terms of Use</a>
                        <!-- /.footer-link -->
                    </li>
                    <!-- /.footer-nav-item -->
                    <li class="footer-nav-item">
                        <a href="privacy-policy.html" class="footer-link">Privacy</a>
                        <!-- /.footer-link -->
                    </li>
                    <!-- /.footer-nav-item -->
                    <li class="footer-nav-item">
                        <a href="#" class="footer-link">Locations</a>
                        <!-- /.footer-link -->
                    </li>
                    <!-- /.footer-nav-item -->
                    <li class="footer-nav-item">
                        <a href="#" class="footer-link">Contact us</a>
                        <!-- /.footer-link -->
                    </li>
                    <!-- /.footer-nav-item -->
                </ul>
                <!-- /.footer-nav -->
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 mt-3 mt-lg-0 mt-md-0">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="footer-download-app">
                            <div class="title">
                                Download Our App On
                            </div>
                            <!-- /.title -->
                            <div class="stores">
                                <div class="store">
                                    <img src="{{asset('assets/icons/apple-icon.svg')}}" alt="image alt"/>
                                </div>
                                <!-- /.store -->
                                <div class="store">
                                    <img src="{{asset('assets/icons/google-play.svg')}}" alt="image alt"/>
                                </div>
                                <!-- /.store -->
                                <div class="store">
                                    <img src="{{asset('assets/icons/window.svg')}}" alt="image alt"/>
                                </div>
                                <!-- /.store -->
                            </div>
                            <!-- /.stores -->
                        </div>
                        <!-- /.footer-download-app -->
                    </div>
                    <!-- /.col-lg-6 col-md-6 col-sm-12 -->
                    <div class="col-lg-6 col-md-6 col-sm-12 mt-3 mt-lg-0 mt-md-0">
                        <div class="footer-payment-methods">
                            <div class="title">
                                We Accept Payment
                            </div>
                            <!-- /.title -->
                            <div class="payments">
                                <div class="payment">
                                    <img src="{{asset('assets/icons/payment-visa.svg')}}" alt="payment icon"/>
                                </div>
                                <!-- /.payment -->
                                <div class="payment">
                                    <img src="{{asset('assets/icons/payment-mastercard.svg')}}" alt="payment icon"/>
                                </div>
                                <!-- /.payment -->
                            </div>
                            <!-- /.payments -->
                        </div>
                        <!-- /.footer-payment-methods -->
                    </div>
                    <!-- /.col-lg-6 col-md-6 col-sm-12 -->
                </div>
                <!-- /.row -->
                <div class="row mt-3">
                    <div class="col-12">
                        <div class="small text-uppercase font-weight-bold">
                            Note
                        </div>
                        <div class="small mt-2">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.                    </div>
                        <!-- /.small -->
                    </div>
                    <!-- /.col-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.col-lg-6 col-md-6 col-sm-12 -->
        </div>
        <!-- /.row -->
        <div class="row mt-5">
            <div class="col-12 ">
                <div class="small text-muted">
                    Copyright 2020 ©
                </div>
                <!-- /.small -->
            </div>
            <!-- /.col-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->

</footer>
