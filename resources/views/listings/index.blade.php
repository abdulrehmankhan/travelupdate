@extends('backoffice.master')
@section('content')
        <!--== breadcrumbs ==-->
        <div class="sb2-2-2">
            <ul>
                <li><a href="#"><i class="fa fa-home" aria-hidden="true"></i> Home</a> </li>
                <li class="active-bre"><a href="#">All Categories</a> </li>
                <li class="page-back"><a href="#"><i class="fa fa-backward" aria-hidden="true"></i>Back</a> </li>
            </ul>
        </div>
        <div class="tz-2 tz-2-admin">
            <div class="tz-2-com tz-2-main">
                <div class="split-row">
                    <div class="col-md-12">
                        <div class="box-inn-sp ad-inn-page">
                            <div class="tab-inn ad-tab-inn">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>Listing</th>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Payment</th>
                                            <th>Listing Type</th>
                                            <th>Status</th>
                                            <th>View</th>
                                            <th>Edit</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($listings as $key=>$listing)
                                        <tr>
                                            <td><span class="list-img">
                                                     @foreach($listing->pictures as $picture)
                                                        @if($picture->type==1)
                                                            <div class="col-md-4">
                                                                    <img src="{{asset('hotels/'.$picture->name)}}" width="350">
                                                                </div>
                                                            @break
                                                        @endif
                                                    @endforeach
                                                </span> </td>
                                            <td><a href="#"><span class="list-enq-name">{{$listing->name}}</span><span class="list-enq-city">{{$listing->category->name}},@foreach($listing->category->locations as $location){{$location->name}},@endforeach {{$listing->category->locations[0]->cities->country->name}}</span></a> </td>
                                            <td>+01 3214 6522</td>
                                            <td> <span class="label label-primary">Pending</span> </td>
                                            <td> <span class="label label-danger">Premium</span> </td>
                                            <td> <span class="label label-primary">Pending</span> </td>
                                            <td> <a href="{{route('listings.show',$listing->slug)}}"><i class="fa fa-eye"></i></a> </td>
                                            <td> <a href="{{route('listings.edit',$listing->id)}}"><i class="fa fa-edit"></i></a> </td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="admin-pag-na">
                            {{ $listings->links() }}
                        </div>
                    </div>
                </div>

                <!-- /.card-body -->
            </div>
        </div>
@endsection
@section('page_level_scripts')
<script>
    $(document).ready(function (){
    $('#hotels').DataTable({
        processing: true,
        serverSide: true,
        responsive:true,
        ajax: '{{ route('hotels.ajax') }}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'address', name: 'address' },
            { data: 'price', name: 'price' },
            { data: 'action', name: 'action' }
        ]
    })
    })
</script>
@endsection