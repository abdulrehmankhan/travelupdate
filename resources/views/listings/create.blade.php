@extends('backoffice.master')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Dashboard v2</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Listings/Add</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Info boxes -->
    <div class="card card-primary">
        <div class="card-body">
            <div class="split-row">
                <div class="col-md-12">
                    <div class="box-inn-sp ad-inn-page">
                        <div class="tab-inn ad-tab-inn">
                            <div class="hom-cre-acc-left hom-cre-acc-right">
                                <div class="">
                                    <form action="{{route('listings.store')}}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <input id="list_name" name="name" type="text" class="validate">
                                                <label for="list_name">Listing Title</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <input id="list_phone" name="phone" type="text" class="validate">
                                                <label for="list_phone">Phone</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <input id="email" name="email" type="email" class="validate">
                                                <label for="email">Email</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <input id="list_addr" name="address" type="text" class="validate">
                                                <label for="list_addr">Address</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <input id="list_addr" name="price" type="number" class="validate">
                                                <label for="list_addr">Price</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <select name="type">
                                                    <option value="" disabled selected>Listing Type</option>
                                                    <option value="1">Free</option>
                                                    <option value="2">Premium</option>
                                                    <option value="3">Premium Plus</option>
                                                    <option value="4">Ultra Premium Plus</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <select name="city_id">
                                                    <option value="" disabled selected>Choose your city</option>
                                                    <option value="1">Kyoto</option>
                                                    <option value="2">Charleston</option>
                                                    <option value="3">Florence</option>
                                                    <option value="">Rome</option>
                                                    <option value="">Mexico City</option>
                                                    <option value="">Barcelona</option>
                                                    <option value="">San Francisco</option>
                                                    <option value="">Chicago</option>
                                                    <option value="">Paris</option>
                                                    <option value="">Tokyo</option>
                                                    <option value="">Beijing</option>
                                                    <option value="">Jerusalem</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <select name="category_id">
                                                    <option value="" disabled selected>Select Category</option>
                                                    @foreach($categories as $category)
                                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
{{--                                        <div class="row">--}}
{{--                                            <div class="input-field col s12">--}}
{{--                                                <select multiple>--}}
{{--                                                    <option value="" disabled selected>Opening Days</option>--}}
{{--                                                    <option value="">All Days</option>--}}
{{--                                                    <option value="">Monday</option>--}}
{{--                                                    <option value="">Tuesday</option>--}}
{{--                                                    <option value="">Wednesday</option>--}}
{{--                                                    <option value="">Thursday</option>--}}
{{--                                                    <option value="">Friday</option>--}}
{{--                                                    <option value="">Saturday</option>--}}
{{--                                                    <option value="">Sunday</option>--}}
{{--                                                </select>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="row">--}}
{{--                                            <div class="input-field col s6">--}}
{{--                                                <select>--}}
{{--                                                    <option value="" disabled selected>Open Time</option>--}}
{{--                                                    <option value="">12:00 AM</option>--}}
{{--                                                    <option value="">01:00 AM</option>--}}
{{--                                                    <option value="">02:00 AM</option>--}}
{{--                                                    <option value="">03:00 AM</option>--}}
{{--                                                    <option value="">04:00 AM</option>--}}
{{--                                                    <option value="">05:00 AM</option>--}}
{{--                                                    <option value="">06:00 AM</option>--}}
{{--                                                    <option value="">07:00 AM</option>--}}
{{--                                                    <option value="">08:00 AM</option>--}}
{{--                                                    <option value="">09:00 AM</option>--}}
{{--                                                    <option value="">10:00 AM</option>--}}
{{--                                                    <option value="">11:00 AM</option>--}}
{{--                                                    <option value="">12:00 PM</option>--}}
{{--                                                    <option value="">01:00 PM</option>--}}
{{--                                                    <option value="">02:00 PM</option>--}}
{{--                                                    <option value="">03:00 PM</option>--}}
{{--                                                    <option value="">04:00 PM</option>--}}
{{--                                                    <option value="">05:00 PM</option>--}}
{{--                                                    <option value="">06:00 PM</option>--}}
{{--                                                    <option value="">07:00 PM</option>--}}
{{--                                                    <option value="">08:00 PM</option>--}}
{{--                                                    <option value="">09:00 PM</option>--}}
{{--                                                    <option value="">10:00 PM</option>--}}
{{--                                                    <option value="">11:00 PM</option>--}}
{{--                                                </select>--}}
{{--                                            </div>--}}
{{--                                            <div class="input-field col s6">--}}
{{--                                                <select>--}}
{{--                                                    <option value="" disabled selected>Closing Time</option>--}}
{{--                                                    <option value="">12:00 AM</option>--}}
{{--                                                    <option value="">01:00 AM</option>--}}
{{--                                                    <option value="">02:00 AM</option>--}}
{{--                                                    <option value="">03:00 AM</option>--}}
{{--                                                    <option value="">04:00 AM</option>--}}
{{--                                                    <option value="">05:00 AM</option>--}}
{{--                                                    <option value="">06:00 AM</option>--}}
{{--                                                    <option value="">07:00 AM</option>--}}
{{--                                                    <option value="">08:00 AM</option>--}}
{{--                                                    <option value="">09:00 AM</option>--}}
{{--                                                    <option value="">10:00 AM</option>--}}
{{--                                                    <option value="">11:00 AM</option>--}}
{{--                                                    <option value="">12:00 PM</option>--}}
{{--                                                    <option value="">01:00 PM</option>--}}
{{--                                                    <option value="">02:00 PM</option>--}}
{{--                                                    <option value="">03:00 PM</option>--}}
{{--                                                    <option value="">04:00 PM</option>--}}
{{--                                                    <option value="">05:00 PM</option>--}}
{{--                                                    <option value="">06:00 PM</option>--}}
{{--                                                    <option value="">07:00 PM</option>--}}
{{--                                                    <option value="">08:00 PM</option>--}}
{{--                                                    <option value="">09:00 PM</option>--}}
{{--                                                    <option value="">10:00 PM</option>--}}
{{--                                                    <option value="">11:00 PM</option>--}}
{{--                                                </select>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <div class="row"> </div>
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <textarea id="textarea1" name="short_description" class="materialize-textarea"></textarea>
                                                <label for="textarea1">Listing Short Descriptions</label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="input-field col s12">
                                                <textarea id="textarea1" name="long_description" class="materialize-textarea"></textarea>
                                                <label for="textarea1">Listing Long Descriptions</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="db-v2-list-form-inn-tit">
                                                <h5>Social Media Informations:</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <input type="text" name="fb_link" class="validate">
                                                <label>www.facebook.com/directory</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <input type="text" name="google_link" class="validate">
                                                <label>www.googleplus.com/directory</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <input type="text" class="validate" name="twitter_link">
                                                <label>www.twitter.com/directory</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="db-v2-list-form-inn-tit">
                                                <h5>Listing Guarantee:</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <select name="guarantee">
                                                    <option value="" disabled selected>Select Service Guarantee</option>
                                                    <option value="1">Upto 2 month of service</option>
                                                    <option value="2">Upto 6 month of service</option>
                                                    <option value="3">Upto 1 year of service</option>
                                                    <option value="4">Upto 2 year of service</option>
                                                    <option value="5">Upto 5 year of service</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <select name="are_you_pro">
                                                    <option value="" disabled selected>Are you a Professionals for this service?</option>
                                                    <option value="1">Yes</option>
                                                    <option value="2">No</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <select name="insurance">
                                                    <option value="" disabled selected>Insurance Limits</option>
                                                    <option value="1">Upto $5,000</option>
                                                    <option value="2">Upto $10,000</option>
                                                    <option value="3">Upto $15,000</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="db-v2-list-form-inn-tit">
                                                <h5>Google Map:</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <input type="text" name="map2d_iframe" class="validate">
                                                <label>Past your iframe code here</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="db-v2-list-form-inn-tit">
                                                <h5>360 Degree View:</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <input type="text" name="map3d_iframe" class="validate">
                                                <label>Past your iframe code here</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="db-v2-list-form-inn-tit">
                                                <h5>Cover Image <span class="v2-db-form-note">(image size 1350x500):<span></h5>
                                            </div>
                                        </div>
                                        <div class="row tz-file-upload">
                                            <div class="file-field input-field">
                                                <div class="tz-up-btn"> <span>File</span>
                                                    <input type="file" name="cover"> </div>
                                                <div class="file-path-wrapper db-v2-pg-inp">
                                                    <input class="file-path validate" type="text">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="db-v2-list-form-inn-tit">
                                                <h5>Photo Gallery <span class="v2-db-form-note">(upload multiple photos note:size 750x500):<span></h5>
                                            </div>
                                        </div>
                                        <div class="row tz-file-upload">
                                            <div class="file-field input-field">
                                                <div class="tz-up-btn"> <span>File</span>
                                                    <input type="file" name="other_picture[]" multiple> </div>
                                                <div class="file-path-wrapper db-v2-pg-inp">
                                                    <input class="file-path validate" type="text">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="db-v2-list-form-inn-tit">
                                                <h5>Services Offered <span class="v2-db-form-note">(Enter service name and upload service image note:size 400x250):<span>:</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <select name="features[]" multiple>
                                                    <option value="" readonly="">Select Features</option>
                                                    @foreach($facilities as $facility)
                                                        <option value="{{$facility->id}}">{{$facility->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s12 v2-mar-top-40"> <button type="submit" class="waves-effect waves-light btn-large full-btn">Submit Listing &amp; Pay</button></div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="admin-pag-na">
                        <ul class="pagination list-pagenat">
                            <li class="disabled"><a href="#!!"><i class="material-icons">chevron_left</i></a> </li>
                            <li class="active"><a href="#!">1</a> </li>
                            <li class="waves-effect"><a href="#!">2</a> </li>
                            <li class="waves-effect"><a href="#!">3</a> </li>
                            <li class="waves-effect"><a href="#!">4</a> </li>
                            <li class="waves-effect"><a href="#!">5</a> </li>
                            <li class="waves-effect"><a href="#!">6</a> </li>
                            <li class="waves-effect"><a href="#!">7</a> </li>
                            <li class="waves-effect"><a href="#!">8</a> </li>
                            <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
            </div>
    </section>
@endsection
@section('page_level_scripts')
        <script type="text/javascript">
            import Input from "../../js/Jetstream/Input";
            import Button from "../../js/Jetstream/Button";
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 6000
            });
            export default {
                components: {Button, Input}
            }
        </script>

        <script type="text/javascript">
            // $('.toastrDefaultSuccess').click(function() {
                toastr.{{Session::get('icon')}}("{{Session::get('message')}}");
            // });
                </script>

@endsection