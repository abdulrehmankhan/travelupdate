@extends('backoffice.master')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Dashboard v2</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard v2</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Info boxes -->
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Add Hotel Availability</h3>
                </div>
                <div class="card-body">
                    <form action="{{route('listings.Availability')}}" method="post" enctype="multipart/form-data" >
                        <div class="row">
                            @csrf
                            <div class="col-6 mb-3">
                                <label>Hotel</label>
                                <select class="form-control" name="hotel_id">
                                    <option value="" readonly="">Select Hotel</option>
                                    @foreach($listings as $hotel)
                                        <option value="{{$hotel->id}}">{{$hotel->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-6 mb-3">
                                <label>Status</label>
                                <select class="form-control" name="status">
                                    <option value="" readonly="">Select Status</option>
                                        <option value="0">Available</option>
                                        <option value="1">Not Available</option>
                                </select>
                            </div>
                            <div class="col-6 mb-3">
                                <label>Check In Date</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    <input type="date" class="form-control" name="checkin_date" placeholder="price">
                                </div>
                            </div>
                            <div class="col-6 mb-3">
                                <label>Check Out Date</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    <input type="date" class="form-control" name="checkout_date" placeholder="price">
                                </div>
                            </div>
                            <div class="col-12 mb-3">
                                <input type="submit" class="btn btn-primary col-12 float-right" value="Add New">
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </section>
@endsection