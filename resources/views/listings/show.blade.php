@extends('backoffice.master')
@section('content')
    <div class="sb2-2-2">
        <ul>
            <li><a href="#"><i class="fa fa-home" aria-hidden="true"></i> Home</a> </li>
            <li class="active-bre"><a href="#"> View Listing</a> </li>
            <li class="page-back"><a href="#"><i class="fa fa-backward" aria-hidden="true"></i> Back</a> </li>
        </ul>
    </div>
    <div class="tz-2 tz-2-admin">
        <div class="tz-2-com tz-2-main">
            <h4>View Listing</h4> <a class="dropdown-button drop-down-meta drop-down-meta-inn" href="#" data-activates="dr-list"><i class="material-icons">more_vert</i></a>
            <ul id="dr-list" class="dropdown-content">
                <li><a href="{{route('listings.create')}}">Add New</a> </li>
                <li><a href="{{route('listings.edit',$hotel->id)}}">Edit</a> </li>
                <li class="divider"></li>
{{--                <li><a href="#!"><i class="material-icons">delete</i>Delete</a> </li>--}}
{{--                <li><a href="#!"><i class="material-icons">subject</i>View All</a> </li>--}}
{{--                <li><a href="#!"><i class="material-icons">play_for_work</i>Download</a> </li>--}}
            </ul>
            <!-- Dropdown Structure -->
            <div class="split-row">
                <div class="col-md-12">
                    <div class="box-inn-sp ad-inn-page">
                        <div class="tab-inn ad-tab-inn">
                            <table class="responsive-table bordered">
                                <tbody>
                                <tr>
                                    <td>Listing</td>
                                    <td>:</td>
                                    <td>{{$hotel->name}}</td>
                                </tr>
                                <tr>
                                    <td>Location</td>
                                    <td>:</td>
                                    <td>@foreach($hotel->category->locations as $location){{$location->name}},@endforeach</td>
                                </tr>
                                <tr>
                                    <td>Eamil</td>
                                    <td>:</td>
                                    <td>{{$hotel->email!='' ? $hotel->email:''}}</td>
                                </tr>
                                <tr>
                                    <td>Phone</td>
                                    <td>:</td>
                                    <td>{{$hotel->phone!='' ? $hotel->phone:''}}</td>
                                </tr>
{{--                                <tr>--}}
{{--                                    <td>Date of Join</td>--}}
{{--                                    <td>:</td>--}}
{{--                                    <td>{{$}}</td>--}}
{{--                                </tr>--}}
{{--                                <tr>--}}
{{--                                    <td>Expairy Date</td>--}}
{{--                                    <td>:</td>--}}
{{--                                    <td>03 Jun 2018</td>--}}
{{--                                </tr>--}}
                                <tr>
                                    <td>Listing Type</td>
                                    <td>:</td>
                                    <td><span class="label label-danger">{{$hotel->type!=''? $hotel->type:''}}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Status</td>
                                    <td>:</td>
                                    <td><span class="label label-{{$hotel->status==1? 'success':'danger'}}">{{$hotel->status==1? 'Active':'Disable'}}</span> </td>
                                </tr>
                                <tr>
                                    <td>Payment</td>
                                    <td>:</td>
                                    <td><span class="label label-{{$hotel->payment== 1 ? 'primary':'success'}}">{{$hotel->payment== 1 ? 'Pending':'Done'}}</span> </td>
                                </tr>
{{--                                <tr>--}}
{{--                                    <td>User Name</td>--}}
{{--                                    <td>:</td>--}}
{{--                                    <td>{{$hotel->}}</td>--}}
{{--                                </tr>--}}
                                <tr>
                                    <td>Service Guarantee</td>
                                    <td>:</td>
                                    <td>
                                        @foreach($hotel->facilities as $facility)
                                        <span class="label label-success">{{$facility->name}}</span>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <td>Listing Completion</td>
                                    <td>:</td>
                                    <td><span class="label label-primary">84%</span> </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
<script>
    import Input from "../../../vendor/laravel/jetstream/stubs/inertia/resources/js/Jetstream/Input";
    export default {
        components: {Input}
    }
</script>