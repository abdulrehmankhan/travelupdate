@extends('backoffice.master')
@section('content')
    <!--== breadcrumbs ==-->
    <div class="sb2-2-2">
        <ul>
            <li><a href="#"><i class="fa fa-home" aria-hidden="true"></i> Home</a> </li>
            <li class="active-bre"><a href="#">All Categories</a> </li>
            <li class="page-back"><a href="#"><i class="fa fa-backward" aria-hidden="true"></i>Back</a> </li>
        </ul>
    </div>
    <div class="tz-2 tz-2-admin">
        <div class="tz-2-com tz-2-main">
            <div class="split-row">
                <div class="col-md-12">
                    <div class="box-inn-sp ad-inn-page">
                        <div class="tab-inn ad-tab-inn">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>NAME</th>
                                        <th>LOCATION</th>
                                        <th>ACTION</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($categories as $category)
                                        <tr>
                                            <td><a href="#"><span class="list-enq-name">{{$category->id}}</span></a> </td>
                                            <td><a href="#"><span class="list-enq-name">{{$category->name}}</span></a> </td>
                                            <td><span class="list-enq-city">@foreach($category->locations as $location){{$location->name}},@endforeach</td>
                                            <td> <a href="{{route('categories.edit',$category->id)}}"><i class="fa fa-edit"></i></a> </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="admin-pag-na">
                        {{ $categories->links() }}
                    </div>
                </div>
            </div>

            <!-- /.card-body -->
        </div>
    </div>

@endsection
@section('page_level_scripts')
    <script>
        $(document).ready(function (){
            $('#category').DataTable({
                processing: true,
                serverSide: true,
                responsive:true,
                ajax: '{{ route('category.ajax') }}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'location', name: 'location' },
                    { data: 'action', name: 'action' }
                ]
            })
        })
    </script>
@endsection