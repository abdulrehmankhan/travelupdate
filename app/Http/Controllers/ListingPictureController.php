<?php

namespace App\Http\Controllers;

use App\Models\ListingPicture;
use Illuminate\Http\Request;

class ListingPictureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ListingPicture  $hotelPicture
     * @return \Illuminate\Http\Response
     */
    public function show(ListingPicture $hotelPicture)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ListingPicture  $hotelPicture
     * @return \Illuminate\Http\Response
     */
    public function edit(ListingPicture $hotelPicture)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ListingPicture  $hotelPicture
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ListingPicture $hotelPicture)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ListingPicture  $hotelPicture
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $hotelPicture=ListingPicture::where('id',$request->id)->first();
        $hotelPicture->delete();
        return response()->json('ok');
    }
}
