<?php

namespace App\Http\Controllers;

use App\Models\Listing;
use App\Models\Location;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=Auth::user();
        $locations=Location::all();
        if(Auth::user()->is_admin==1){
            $listing=Listing::where('created_by',Auth::id())->paginate(10);
            $listingCount=Listing::where('created_by',Auth::id())->count();
        return view('Howner.dashboard',compact('locations','listingCount','listing','user'));
        }elseif(Auth::user()->is_admin==2){
            $listing=Listing::where('created_by',Auth::id())->paginate(10);
            $listingCount=Listing::where('created_by',Auth::id())->count();
            return view('Powner.dashboard',compact('locations','listingCount','listing','user'));
        }elseif(Auth::user()->is_admin==3){
            $listing=Listing::where('created_by',Auth::id())->paginate(10);
            $listingCount=Listing::where('created_by',Auth::id())->count();
            return view('user.dashboard',compact('locations','listingCount','listing','user'));
        }elseif(Auth::user()->is_admin==0){
            $listing=Listing::where('id','>',0)->paginate(10);
            $listingCount=Listing::where('id','>',0)->count();
            return view('user.dashboard',compact('locations','listingCount','listing','user'));
        }else{
            return  redirect(route('index','listing'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
    public function profile(){
        $user=Auth::user();
        if($user){
        return view('profile',compact('user'));
        }else{
            return redirect()->back();
        }
    }
    public function profileUpdate(Request $request){
        $user=Auth::user();
       $user->name=$request->username;
       $user->email=$request->email;
       if($request->password) {
           $user->password = Hash::make($request->password);
       }
       $user->dob=Carbon::parse($request->dob)->format('y-m-d');
       $user->phone=$request->phone;
       $user->status=$request->status;
        if ($request->hasFile('profile_picture')) {
            $file_extension = $request->file('profile_picture')->getClientOriginalExtension();
            $file_path = $request->file('profile_picture')->getFilename();
            $filename =  $request->username. '.' . $file_extension;
            Storage::disk('ProfilePicture')->put($filename, \File::get($request->file('profile_picture')));
            $user->profile_photo_path = $filename;
        }
        $user->save();
        return redirect()->back();

    }
}
