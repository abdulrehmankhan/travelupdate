<?php

namespace App\Http\Controllers;

use App\Models\ListingAvailability;
use Illuminate\Http\Request;

class ListingAvailabilityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ListingAvailability  $hotelAvailability
     * @return \Illuminate\Http\Response
     */
    public function show(ListingAvailability $hotelAvailability)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ListingAvailability  $hotelAvailability
     * @return \Illuminate\Http\Response
     */
    public function edit(ListingAvailability $hotelAvailability)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ListingAvailability  $hotelAvailability
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ListingAvailability $hotelAvailability)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ListingAvailability  $hotelAvailability
     * @return \Illuminate\Http\Response
     */
    public function destroy(ListingAvailability $hotelAvailability)
    {
        //
    }
}
