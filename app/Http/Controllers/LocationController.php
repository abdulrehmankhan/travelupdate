<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\City;
use App\Models\Listing;
use App\Models\Location;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param Location $location
     * @return Response
     */
    public function show(Location $location)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Location $location
     * @return Response
     */
    public function edit(Location $location)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Location $location
     * @return Response
     */
    public function update(Request $request, Location $location)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Location $location
     * @return Response
     */
    public function destroy(Location $location)
    {
        //
    }

    public function search(Request $request)
    {
        $results = Listing::with('category.locations')->where('name', 'like', '%' . $request->keyword . '%')->get();
        $html = view('frontend.searchlist', compact('results'))->render();
        return $html;
    }

    public function List(Request $request)
    {
//        return $request->category_name;
//        $date = explode('-', $request->datefilter);
//        $checkin_date =Carbon::parse($date[0])->toDateString();
//        $checkout_date =Carbon::parse($date[1])->toDateString();
//        $hotels=Listing::with('facilities','pictures','category.locations')
//            ->whereHas('availabilities', function ($q) use ($checkin_date,$checkout_date) {
//                $q->whereBetween('checkin_date',[$checkin_date,$checkout_date])
//                    ->where('status',0);
//            })
//                ->where('category_id',$request->category_id)->get();
        $categorey=Category::where('name',$request->category_name)->first();
        $listings = Listing::with('facilities', 'pictures', 'category.locations', 'availabilities')->where('category_id', $categorey->id)->get();
        return view('frontend.list', compact('listings'));
    }

//    public function viewlist(Request $request)
//    {
//        $hotels = Listing::with('facilities', 'pictures', 'category.locations', 'availabilities')->get();
//        return view('frontend.list', compact('hotels'));
//    }

    public function Categories()
    {
        $categories = Listing::with('category','city')->get();
        return view('frontend.index', compact('categories'));
    }

    public function Listing()
    {
        $listings = Listing::all();
        return view('frontend.alllisting', compact('listings'));
    }

    public function city($slug)
    {
        $city=City::where('name',$slug)->first();
        $listings = Listing::where('city_id',$city->id)->get();
        return view('frontend.alllisting', compact('listings'));
    }
}
