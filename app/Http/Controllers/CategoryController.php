<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories=Category::with('locations')->paginate(12);
        return view('categories.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $locations=Location::all();
        return view('categories.create',compact('locations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        return $request;
        $category=new Category();
        $category->name=$request->name;
        $category->save();
        $category->locations()->sync($request->locations,false);
        $notification = array(
            'message' => 'Category Created Successfully!',
            'icon' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $locations=Location::all();
        return view('categories.edit',compact('category','locations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $category->name=$request->name;
        $category->save();
        $category->locations()->sync($request->locations,false);
        $notification = array(
            'message' => 'Category Updated Successfully!',
            'icon' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }
    public function ajax()
    {
        $categories = Category::with( 'locations')->get();
//        return $categories;

        return Datatables::of($categories)
            ->addColumn('action', function (Category $category) {
                $btn = '<div class="d-flex"><a href="categories/' . $category->id . '/edit" class="btn btn-primary" title="Edit"><i class="fa fa-edit"></i></a><a href="categories/' . $category->id . '" class="btn btn-primary" title="Show"><i class="fa fa-eye"></i></a></div>';
                return $btn;
            })
            ->addColumn('location',function($categories){
                $locationz=[];
                foreach($categories->locations as $location){
                     array_push($locationz,$location->name);
                }
                return $locationz;
            })
            ->rawColumns(['location','action'])
            ->make(true);
    }
}
