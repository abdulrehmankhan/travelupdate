<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Facilities;
use App\Models\Listing;
use App\Models\ListingAvailability;
use App\Models\ListingPicture;
use App\Models\Location;
use Carbon\Carbon;
use Exception;
use File;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class ListingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $user=Auth::user();
        $listings=Listing::with('facilities','pictures','category.locations.cities.country')->where('created_by',Auth::id())->paginate(12);
        return view('listings.index',compact('listings','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $user=Auth::user();
        $categories = Category::all();
        $facilities = Facilities::all();
        return view('listings.create', compact('categories', 'facilities','user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
//        $request->validate([
//            'name' => 'required',
//            'short_description' => 'required',
//            'long_description' => 'required',
//            'category_id' => 'required',
//            'address' => 'required'
//        ]);
        try {
            $hotel = new Listing();
            $hotel->name = $request->name;
            $hotel->slug = Str::of($request->name)->slug('-');
            $hotel->short_description = $request->short_description;
            $hotel->long_description = $request->long_description;
            $hotel->address = $request->address;
            $hotel->price = $request->price;
            $hotel->category_id = $request->category_id;
            $hotel->city_id = $request->city_id;
            $hotel->created_by = Auth::id();
            $hotel->type = Auth::user()->is_admin;
            $hotel->save();
            if ($request->hasFile('cover')) {
                $picture = new ListingPicture();
                $file_extension = $request->file('cover')->getClientOriginalExtension();
                $file_path = $request->file('cover')->getFilename();
                $filename = $request->name . '.' . $file_extension;
                Storage::disk('pictures')->put($filename, File::get($request->file('cover')));
                $picture->name = $filename;
                $picture->type = 1;
                $picture->listing_id = $hotel->id;
                $picture->save();
            }
            if ($request->hasFile('other_picture')) {
                foreach ($request->other_picture as $file) {
                    $picture = new ListingPicture();
                    $file_extension = $file->getClientOriginalExtension();
                    $file_path = $file->getFilename();
                    $filename = $request->name . '.' . $file_extension;
                    Storage::disk('pictures')->put($filename, \File::get($file));
                    $picture->name = $filename;
                    $picture->type = 0;
                    $picture->listing_id = $hotel->id;
                    $picture->save();
                }
            }
            foreach ($request->features as $feature) {
                $hotel->facilities()->attach($feature);
            }
            if($hotel->type==2){
                $notification = array(
                    'message' => 'Property Created Successfully!',
                    'icon' => 'success'
                );
                return redirect()->back()->with($notification);
            }elseif($hotel->type==1){
                $notification = array(
                    'message' => 'Hotel Created Successfully!',
                    'icon' => 'success'
                );
                return redirect()->back()->with($notification);
            }
        } catch (Exception $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'icon' => 'error'
            );
            return redirect()->back()->with($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Listing $hotel
     * @return Response
     */
    public function show($hotel)
    {
        $user=Auth::user();
        $hotel = Listing::with('facilities', 'category.locations', 'pictures', 'availabilities')->where('slug', $hotel)->where('created_by', Auth::id())->first();
        return view('listings.show', compact('hotel','user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Listing $hotel
     * @return Response
     */
    public function edit($hotel)
    {
        $user=Auth::user();
        $categories = Category::all();
        $hotel = Listing::with('facilities', 'category', 'pictures', 'availabilities')->where('id', $hotel)->where('created_by', Auth::id())->first();
        return view('listings.edit', compact('hotel', 'categories','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Listing $hotel
     * @return Response
     */
    public function update(Request $request, Listing $hotel)
    {
        $hotel = Listing::with('facilities', 'category', 'pictures', 'availabilities')->where('id', $request->hotel_id)->where('created_by', Auth::id())->first();
        $hotel->name = $request->name;
        $hotel->slug = Str::of($request->name)->slug('-');
        $hotel->short_description = $request->short_description;
        $hotel->long_description = $request->long_description;
        $hotel->address = $request->address;
        $hotel->price = $request->price;
        $hotel->category_id = $request->category_id;
        $hotel->city_id = $request->city_id;
        $hotel->created_by = Auth::user()->id;
        $hotel->save();
        if ($request->hasFile('cover')) {
            $picture = new ListingPicture();
            $file_extension = $request->file('cover')->getClientOriginalExtension();
            $file_path = $request->file('cover')->getFilename();
            $filename = Str::random(1, 2) . '.' . $file_extension;
            Storage::disk('pictures')->put($filename, File::get($request->file('cover')));
            $picture->name = $filename;
            $picture->type = 1;
            $picture->listing_id = $hotel->id;
            $picture->save();
        }
        if ($request->hasFile('other_picture')) {
            foreach ($request->other_picture as $file) {
                $picture = new ListingPicture();
                $file_extension = $file->getClientOriginalExtension();
                $file_path = $file->getFilename();
                $filename = Str::random(1, 9) . '.' . $file_extension;
                Storage::disk('pictures')->put($filename, File::get($file));
                $picture->name = $filename;
                $picture->type = 0;
                $picture->listing_id = $hotel->id;
                $picture->save();
            }
        }
        foreach ($request->checkin_date as $key => $date) {
            $availability = ListingAvailability::where('hotel_id', $hotel->id)->where('id', $request->availability_id[$key])->first();
            $checkin_date = Carbon::parse($date)->toDateString();
            $checkout_date = Carbon::parse($request->checkout_date[$key])->toDateString();
            $availability->checkin_date = $checkin_date;
            $availability->checkout_date = $checkout_date;
            $availability->status = $request->status[$key];
            $availability->hotel_id = $request->hotel_id;
            $availability->save();
        }

        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Listing $hotel
     * @return Response
     */
    public function destroy(Listing $hotel)
    {
        //
    }

    public function Availability(Request $request)
    {

        $listing=Listing::where('created_by',Auth::id())->first();
        if($listing){
            $availability = new ListingAvailability();
            $availability->checkin_date = Carbon::parse($request->checkin_date)->toDateString();
            $availability->checkout_date = Carbon::parse($request->checkout_date)->toDateString();
            $availability->listing_id = $request->hotel_id;
            $availability->status = $request->status;
            $availability->save();
            return redirect()->back();
        }else{
            return redirect()->back();
        }

    }

    public function makeavailable()
    {
        $listings = Listing::where('created_by', Auth::id())->get();
        return view('listings.available', compact('listings'));
    }

    public function detail($slug)
    {
        $user=Auth::user();
        $hotel = Listing::with('facilities', 'category.locations', 'pictures')->where('slug', $slug)->first();
        $listings = Listing::limit(3)->inRandomOrder()->get();
        return view('frontend.detail', compact('hotel','listings','user'));
    }

    public function ajax()
    {
        $listings = Listing::with('facilities', 'category.locations', 'pictures')->where('created_by', Auth::id())->get();
        return Datatables::of($listings)
            ->addColumn('action', function (Listing $hotel) {
                $btn = '<div class="d-flex"><a href="../listings/' . $hotel->id . '/edit" class="btn btn-primary" title="Edit"><i class="fa fa-edit"></i></a><a href="../listings/' . $hotel->id . '" class="btn btn-primary" title="Show"><i class="fa fa-eye"></i></a></div>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}
