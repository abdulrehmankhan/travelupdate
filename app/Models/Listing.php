<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Listing extends Model
{
    use HasFactory;
    public function facilities(){
        return $this->belongsToMany(Facilities::class);
    }
    public function pictures(){
        return $this->hasMany(ListingPicture::class);
    }
    public function availabilities(){
        return $this->hasMany(ListingAvailability::class,'listing_id');
    }
    public function category(){
        return $this->belongsTo(Category::class);
    }
    public function city(){
        return $this->belongsTo(City::class);
    }
}
