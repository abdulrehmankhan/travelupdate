<?php

use App\Http\Controllers\CategoryController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LocationController;
use \App\Http\Controllers\CountryController;
use \App\Http\Controllers\CityController;
use \App\Http\Controllers\ListingController;
use \App\Http\Controllers\UserController;
use \App\Http\Controllers\ListingPictureController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Auth::routes();
Route::get('/', [LocationController::class,'Categories'])->name('index');
Route::get('/all/listing', [LocationController::class,'Listing'])->name('listing');
Route::get('/all/city/{slug}', [LocationController::class,'city'])->name('cityListing');
Route::group(['prefix' => 'hotel', 'middleware' => ['auth', 'verified']],function () {

    Route::get('/dashboard',[UserController::class,'index'])->name('Howner');
    Route::get('listings/index',[ListingController::class,'index'])->name('listings.index');
    Route::get('listings/create',[ListingController::class,'create'])->name('listings.create');
    Route::post('listings/store',[ListingController::class,'store'])->name('listings.store');
    Route::get('listings/{id}/edit',[ListingController::class,'edit'])->name('listings.edit');
    Route::patch('listings/{id}',[ListingController::class,'update'])->name('listings.update');
    Route::delete('listings/{id}',[ListingController::class,'destroy'])->name('listings.destroy');
    Route::get('listings/{id}',[ListingController::class,'show'])->name('listings.show');
    Route::post('availability/hotels',[ListingController::class,'Availability'])->name('listings.Availability');
    Route::get('available/hotels/',[ListingController::class,'makeavailable'])->name('hotels.makeAvailability');
    Route::post('photos/delete',[ListingPictureController::class,'destroy'])->name('photos.delete');
    Route::get('/hotel1/ajax',[ListingController::class,'ajax'])->name('hotels.ajax');
    //ya get wala ha oper wala ya nichy wala nechay wala

});
Route::group(['prefix' => 'property', 'middleware' => ['auth', 'verified']],function () {

    Route::get('/dashboard',[UserController::class,'index'])->name('Powner');
    Route::get('listings/index',[ListingController::class,'index'])->name('listings.index');
    Route::get('listings/create',[ListingController::class,'create'])->name('listings.create');
    Route::post('listings/store',[ListingController::class,'store'])->name('listings.store');
    Route::get('listings/{id}/edit',[ListingController::class,'edit'])->name('listings.edit');
    Route::patch('listings/{id}',[ListingController::class,'update'])->name('listings.update');
    Route::delete('listings/{id}',[ListingController::class,'destroy'])->name('listings.destroy');
    Route::get('listings/{id}',[ListingController::class,'show'])->name('listings.show');
    Route::post('availability/hotels',[ListingController::class,'Availability'])->name('listings.Availability');
    Route::get('available/hotels/',[ListingController::class,'makeavailable'])->name('listings.makeAvailability');
    Route::post('photos/delete',[ListingPictureController::class,'destroy'])->name('photos.delete');
    Route::get('/hotel1/ajax',[ListingController::class,'ajax'])->name('hotels.ajax');

});
Route::group(['prefix' => 'user', 'middleware' => ['auth', 'verified']],function () {

    Route::get('/dashboard',[UserController::class,'index'])->name('userDashboard');
    Route::get('listings/index',[ListingController::class,'index'])->name('listings.index');
    Route::get('listings/create',[ListingController::class,'create'])->name('listings.create');
    Route::post('listings/store',[ListingController::class,'store'])->name('listings.store');
    Route::get('listings/{id}/edit',[ListingController::class,'edit'])->name('listings.edit');
    Route::patch('listings/{id}',[ListingController::class,'update'])->name('listings.update');
    Route::delete('listings/{id}',[ListingController::class,'destroy'])->name('listings.destroy');
    Route::get('listings/{id}',[ListingController::class,'show'])->name('listings.show');

    Route::post('availability/hotels',[ListingController::class,'Availability'])->name('listings.Availability');
    Route::get('available/hotels/',[ListingController::class,'makeavailable'])->name('listings.makeAvailability');
    Route::post('photos/delete',[ListingPictureController::class,'destroy'])->name('photos.delete');
    Route::get('/hotel1/ajax',[ListingController::class,'ajax'])->name('hotels.ajax');
});
Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'verified']],function () {

    Route::get('/dashboard',[UserController::class,'index'])->name('adminDashboard');
    Route::get('listings/index',[ListingController::class,'index'])->name('listings.index');
    Route::get('listings/create',[ListingController::class,'create'])->name('listings.create');
    Route::post('listings/store',[ListingController::class,'store'])->name('listings.store');
    Route::get('listings/{id}/edit',[ListingController::class,'edit'])->name('listings.edit');
    Route::patch('listings/{id}',[ListingController::class,'update'])->name('listings.update');
    Route::delete('listings/{id}',[ListingController::class,'destroy'])->name('listings.destroy');
    Route::get('listings/{id}',[ListingController::class,'show'])->name('listings.show');
    Route::post('availability/hotels',[ListingController::class,'Availability'])->name('listings.Availability');
    Route::get('available/hotels/',[ListingController::class,'makeavailable'])->name('listings.makeAvailability');
    Route::post('photos/delete',[ListingPictureController::class,'destroy'])->name('photos.delete');
    Route::get('/hotel1/ajax',[ListingController::class,'ajax'])->name('hotels.ajax');
    Route::resource('countries',CountryController::class);
    Route::resource('cities',CityController::class);
    Route::resource('location',LocationController::class);
    Route::resource('categories',CategoryController::class);
    Route::get('/category/ajax',[CategoryController::class,'ajax'])->name('category.ajax');
});

Route::get('list/{slug}',[ListingController::class,'detail'])->name('detail');
Route::get('hotelz/{slug}',[LocationController::class,'list'])->name('viewall');
//Route::get('hotelz',[LocationController::class,'viewlist'])->name('viewlist');
Route::get('ajaxSearch',[LocationController::class,'search'])->name('ajaxSearch');
Route::get('search',[LocationController::class,'list'])->name('search');
Route::get('/profile',[UserController::class,'profile'])->name('profile');
Route::post('/profile/update',[UserController::class,'profileUpdate'])->name('profileUpdate');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
